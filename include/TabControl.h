/** @file TabControl.h
    @brief Interface to the TabControl class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_TABCONTROL_H
#define LIBPF_LOC_TABCONTROL_H

class QLabel;
class QLineEdit;
class QPushButton;
class QTimer;

#include <QWidget>
#include <QIcon>

#ifdef Q_OS_WIN
#define NOMINMAX

#include "vservices.h"

using namespace voidrealms::win32;
#define SERVICE_STATUS_CONTINUE_PENDING "SERVICE_CONTINUE_PENDING"
#define SERVICE_STATUS_PAUSE_PENDING "SERVICE_PAUSE_PENDING"
#define SERVICE_STATUS_SERVICE_PAUSED "SERVICE_PAUSED"
#define SERVICE_STATUS_SERVICE_RUNNING "SERVICE_RUNNING"
#define SERVICE_STATUS_SERVICE_START_PENDING "SERVICE_START_PENDING"
#define SERVICE_STATUS_SERVICE_STOP_PENDING "SERVICE_STOP_PENDING"
#define SERVICE_STATUS_SERVICE_STOPPED "SERVICE_STOPPED"
#endif // Q_OS_WIN

class TabControl : public QWidget {
  Q_OBJECT

public:
  TabControl(QWidget *parent = 0);
  ~TabControl();

private:
  QIcon m_iconStart;
  QIcon m_iconStop;
  
#ifdef Q_OS_WIN
  VServices m_ServiceManager;
#endif // Q_OS_WIN

  QLabel* m_pLabelServiceStatus;
  QLineEdit* m_pLineEditServiceStatus;
  QPushButton* m_pButtonStart;
  QPushButton* m_pButtonStop;
  bool m_bServiceStarted;
  QTimer *m_pTimer;
  void timerEvent(QTimerEvent * /* e */);
  QString GetServiceStatusAsString(const QString & sStatus);
public slots:;
  void UpdateServices();
  void StartService();
  void StopService();
}; // class TabControl

#endif // LIBPF_LOC_TABCONTROL_H
