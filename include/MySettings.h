/** @file MySettings.h
    @brief Interface to the MySettings class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_MYSETTINGS_H
#define LIBPF_LOC_MYSETTINGS_H

#include <QSettings>

class MySettings : public QSettings {
public:
  MySettings(void);
  QString schemaFileName(void) const;
}; // class MySettings

#endif // LIBPF_LOC_MYSETTINGS_H
