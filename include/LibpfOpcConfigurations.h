/** @file LibpfOPCConfigurations.h
    @brief Interface to the LibpfOPCConfigurations class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_OPCCONFIGURATIONS_H
#define LIBPF_LOC_OPCCONFIGURATIONS_H

#include <QString>

class LibpfOPCConfigurations {
public:
  QString m_sHomePath;
  QString m_sKernelPath;
  QString m_sCLSIDOfOPCServer;
  int m_iSleepTime;
  QString m_sTracePipe;
  QString m_sTraceServer;
  int m_iGlobalVerbosity;
  int m_iFlashVerbosity;
  int m_iReadOnlyMode;
  int m_iMaxErrorCount;
public:

  LibpfOPCConfigurations(void);
  LibpfOPCConfigurations(const LibpfOPCConfigurations & other);
  const LibpfOPCConfigurations & operator==(const LibpfOPCConfigurations & other);
  ~LibpfOPCConfigurations(void);
}; // class LibpfOPCConfigurations

#endif  // LIBPF_LOC_OPCCONFIGURATIONS_H
