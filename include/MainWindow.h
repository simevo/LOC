/** @file MainWindow.h
    @brief Interface to the MainWindow class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_MAINWINDOW_H
#define LIBPF_LOC_MAINWINDOW_H

#include <QMainWindow>

class MainWidget;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QString &Path, QWidget *parent = 0, Qt::WindowFlags flags = 0);
  ~MainWindow();
  void closeEvent(QCloseEvent* event);
private:
  QString m_sAppPath;
  MainWidget *m_pMainWidget;
}; // class MainWindow

#endif // LIBPF_LOC_MAINWINDOW_H
