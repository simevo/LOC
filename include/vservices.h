/** @file vqtconvert.h
    @brief Interface to the Wrapper for the Windows Services

    This file is part of LOC, the LIBPF OPC Configurator

    Based on:

      Wrapper for the Windows Services
      Version: 1.0
      Modified on: 8-20-2009
      Created with: QT 4.5 and QT Creator 1.2
      Tested on: Windows XP SP3
      Bryan Cairns

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_VSERVICES_H
#define LIBPF_LOC_VSERVICES_H

#include <QString>
#include <QStringList>
#include <QList>

#include "windows.h"

namespace voidrealms {
namespace win32 {
class VServices {
private:
  QString mComputername;
  SC_HANDLE mManager;

  //Opens the service manager on the specified computer
  bool OpenManager();

  //Closes the service manager
  bool CloseManager();

public:

  //Enumerations
  enum StartType {StartType_Automatic, StartType_Manual, StartType_Disabled};

  //Default Constructor
  VServices();

  //Constructor for remote computers
  VServices(QString Computername);

  //Returns a QStringList filled with installed service names
  QStringList getServicesStringList();

  //Gets the services display name
  QString getDisplayName(QString ServiceName);

  //Returns a QString with the services status
  QString getStatus(QString ServiceName);

  //Returns a QString with the services start type
  QString getStartType(QString ServiceName);

  //Start a Service with Arguments
  bool Start(QString ServiceName, QStringList Arguments);

  //Start a Service
  bool Start(QString ServiceName);

  //Stop a Service
  bool Stop(QString ServiceName);

  //Set the Start Type
  bool setStartType(QString ServiceName,StartType Type);

  //Returns a QString with the services startup account name
  QString getStartAccount(QString ServiceName);

  //Set the services startup account name and password
  bool setStartAccount(QString ServiceName, QString Username, QString Password);

  //Removes a service
  bool ServiceDelete(QString ServiceName);

  //Creates a service
  bool ServiceCreate(QString ServiceName,QString DisplayName,QString Description,StartType Type,QString Path,QString Username, QString Password);
}; // class VServices
}     //end voidrealms::win32 namespace
} //end voidrealms namespace

#endif // LIBPF_LOC_VSERVICES_H
