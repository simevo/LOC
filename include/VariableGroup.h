/** @file VariableGroup.h
    @brief Interface to the VariableGroup class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_VARIABLEGROUP_H
#define LIBPF_LOC_VARIABLEGROUP_H

#include <QList>
#include <QString>

#include "Variable.h"

class VariableGroup  {
public:
  QString m_sId;
  QString m_sDescription;
  QString m_sModel;
  bool m_bIsEnabled;
  QList<Variable> m_listVariablesInput;
  QList<Variable> m_listVariablesResult;
  QList<Variable> m_listVariablesConstant;
public:
  VariableGroup(void);
  ~VariableGroup();
  VariableGroup(const VariableGroup &other);
  VariableGroup &operator=(const VariableGroup &other);
}; // class VariableGroup

#endif // LIBPF_LOC_VARIABLEGROUP_H
