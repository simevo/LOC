/** @file Appconfig.h
    @brief Interface to the AppConfig class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_APPCONFIG_H
#define LIBPF_LOC_APPCONFIG_H

#include <QString>
#include <QStringList>

class AppConfig {
private:
  static QStringList s_listUOMDefaultValues;
public:
  AppConfig(void);
  ~AppConfig(void);

  static const QString s_sRegPathAppName;
  static const QString s_sRegPathAppVersion;
  static const QString s_sRegPathOrganization;

  // Registry keys
  static const QString s_sRegKey_HomePath;
  static const QString s_sRegKey_KernelPath;
  static const QString s_sRegKey_CLSID;
  static const QString s_sRegKey_SleepTime;
  static const QString s_sRegKey_TracePipe;
  static const QString s_sRegKey_TraceServer;
  static const QString s_sRegKey_VerbosityGlobal;
  static const QString s_sRegKey_VerbosityFlash;
  static const QString s_sRegKey_ReadOnly;
  static const QString s_sRegKey_MaxErrorCount;
  static const QString s_sDefaultVariableGroupFileName;
  static const QString s_sImagesFolder;
  static const QString s_sTempFileForValidating;
  static const int s_iVerbosityMin;
  static const int s_iVerbosityMax;
  static const int s_iErrorCountMin;
  static const int s_iErrorCountMax;

  // Xml settings file related
  static const QString s_sVariableGroupsFileXmlVersion;
  static const QString s_sVariableGroupsFileXmlEncoding;
  static const QString s_sVariableGroupsFileXmlAttribute;
  static const QString s_sXmlProcessingInstruction_StyleSheet;
  static const QString s_sXmlTag_Groups;
  static const QString s_sXmlAttribute_GroupsLink;
  static const QString s_sXmlAttributeValue_GroupsLink;
  static const QString s_sXmlAttribute_GroupsSchema;
  static const QString s_sXmlAttributeValue_GroupsSchema;
  static const QString s_sXmlAttribute_VariableOPCtag;
  static const QString s_sXmlAttribute_VariableDescription;
  static const QString s_sXmlAttribute_VariableTAG;
  static const QString s_sXmlAttribute_VariableUOM;
  static const QString s_sXmlAttribute_VariableMin;
  static const QString s_sXmlAttribute_VariableMax;

  static const QString s_sXmlTag_Group;
  static const QString s_sXmlTag_Input;
  static const QString s_sXmlTag_Result;
  static const QString s_sXmlTag_Constant;

  static const QString s_sXmlAttribute_GroupId;
  static const QString s_sXmlAttribute_GroupEnabled;
  static const QString s_sXmlAttribute_GroupDescription;
  static const QString s_sXmlAttribute_GroupModel;

  // Service
  static const QString s_sServiceName;
  static const int s_iServiceUpdateInterval;
  static const int s_iSleepTimeMin;
  static const int s_iSleepTimeMax;

  static const QStringList & GetDefaultUOMValuesList();
}; // class AppConfig

#endif  ///LIBPF_LOC_APPCONFIG_H
