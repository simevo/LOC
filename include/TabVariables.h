/** @file TabVariables.h
    @brief Interface to the TabVariables class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_TABVARIABLES_H
#define LIBPF_LOC_TABVARIABLES_H

class QStringList;
class QTableWidget;
class QPushButton;
class QStringListModel;
class QTabWidget;
class QSplitter;
class QIcon;
class QMenu;
class QFileDialog;
class QComboBox;
class QStatusBar;
class QTableWidgetItem;

#include <QWidget>
#include <QFileDialog>
#include <QIcon>

#include "VariableGroup.h"

class TabVariables : public QWidget {
  Q_OBJECT

public:
  TabVariables(QWidget *parent = 0);
  ~TabVariables();
  void SetDefaultVariableGroupFile(const QString & sFileName);
  void LoadDefaultVariableGroupFile();
  bool AskForLeaveUnsavedWork(const QString message);

private:
  QStatusBar* m_pStatusBar;
  QString m_sUnsavedChanges;
  QFileDialog m_FileDialog;
  bool m_bIsRowChanging;
  bool m_bIsGroupChanging;
  void SetData(QList<VariableGroup>* VariableGroups);
  bool m_bIsLoadingData;
  bool m_bIsRowMoving;
  QString m_sDefaultVariableGroupFile;
  QList<VariableGroup> m_VariableGroups;
  QPushButton* m_pButtonImport;
  QPushButton* m_pButtonExport;
  QPushButton* m_pButtonRefresh;
  QPushButton* m_pButtonSave;
  QTableWidget* m_pTableVariableGroups;
  QTableWidget* m_pTableVariableInput;
  QTableWidget* m_pTableVariableResult;
  QTableWidget* m_pTableVariableConstant;
  QMenu* m_pContextMenuVariableGroup;
  QMenu* m_pContextMenuVariableInput;
  QMenu* m_pContextMenuVariableResult;
  QMenu* m_pContextMenuVariableConstant;
  QAction* m_pActionVariableGroupMoveUp;
  QAction* m_pActionVariableGroupMoveDown;
  QAction* m_pActionVariableInputMoveUp;
  QAction* m_pActionVariableInputMoveDown;
  QAction* m_pActionVariableResultMoveUp;
  QAction* m_pActionVariableResultMoveDown;
  QAction* m_pActionVariableConstantMoveUp;
  QAction* m_pActionVariableConstantMoveDown;
  QStringList m_listUOMValues;
  QTabWidget* m_pWidgetVariables;

  QStringListModel* m_pModelGroupVariables;
  QSplitter* m_pSplitter;
  bool m_bIsChanged;
  QIcon m_iconAdd;
  QIcon m_iconRemove;

  /// Drag&Drop
  /// @{
  void dragEnterEvent(QDragEnterEvent *event);
  void dragMoveEvent(QDragMoveEvent *event);
  void dropEvent(QDropEvent *event);
  /// @}

  VariableGroup m_CurrentVariableGroup;

  void SetChangesMade(bool bIsChangesMade);
  void AddNewRowPlaceHolder(QTableWidget* pTableVariable);
  void SetWidgetVariableValue(const VariableGroup & CurrentVariableGroup);
  void RemoveARowFromVariableTable(QTableWidget* pTableVariable, int iIndex);
  void AddNewRowToVariableTable(QTableWidget* pTableVariable);
  void MoveTableRow(QTableWidget* pTable, int iSrcIndex, int iDesIndex);
  void MoveVariableGroup(QList<VariableGroup> & pTarget, int iSrcIndex, int iDesIndex);
  void MoveVariable(QList<Variable> & pTarget, int iSrcIndex, int iDesIndex);
  void UpdateValueChanged(QTableWidget* pTable, int iRowIndex);
  int GetSelectingIndexOnWidget(QTableWidget* pTarget);
  void closeEvent(QCloseEvent *event);

public slots:
  /// Context menus
  /// @{
  void ActiveContextMenuVariableGroup(const QPoint &pos);
  void ActiveContextMenuVariableInput(const QPoint &pos);
  void ActiveContextMenuVariableResult(const QPoint &pos);
  void ActiveContextMenuVariableConstant(const QPoint &pos);

  void HandleActionTriggeredVariableGroupMoveUp();
  void HandleActionTriggeredVariableGroupMoveDown();
  void HandleActionTriggeredVariableInputMoveUp();
  void HandleActionTriggeredVariableInputMoveDown();
  void HandleActionTriggeredVariableResultMoveUp();
  void HandleActionTriggeredVariableResultMoveDown();
  void HandleActionTriggeredVariableConstantMoveUp();
  void HandleActionTriggeredVariableConstantMoveDown();
  void HandleInputVariableUOMValueChanged(const QString & text);
  void HandleResultVariableUOMValueChanged(const QString & text);
  void HandleConstantVariableUOMValueChanged(const QString & text);
  /// @}

  // void VariableGroupContextMenu(const QPoint &pos);
  // void VariableInputContextMenu(const QPoint &pos);
  // void VariableResultContextMenu(const QPoint &pos);
  // void VariableConstantContextMenu(const QPoint &pos);

  void HandleCurrentGroupChanged(QTableWidgetItem * current, QTableWidgetItem * previous);
  void HandleItemClicked(QTableWidgetItem* item);
  void HandleButtonClickedSave();
  void HandleButtonClickedImport();
  void HandleButtonClickedExport();
  void HandleButtonClickedRefresh();
  void HandleGroupSelectionChanged();

  void HandleVariableGroupTableCellClicked(int iRow, int iColumn);
  void HandleVariableGroupVerticalHeaderPressed(int iLogicalIndex);

  void HandleVariableInputVerticalHeaderPressed(int iLogicalIndex);
  void HandleVariableResultVerticalHeaderPressed(int iLogicalIndex);
  void HandleVariableConstantVerticalHeaderPressed(int iLogicalIndex);
  void HandleVariableGroupChanged(int iRow, int iColumn);
  void HandleVariableInputChanged(int iRow, int iColumn);
  void HandleVariableResultChanged(int iRow, int iColumn);
  void HandleVariableConstantChanged(int iRow, int iColumn);
  void HandleCellChanged(QTableWidget* table, int iRow);
}; // class TabVariables

#endif // LIBPF_LOC_TABVARIABLES_H
