/** @file TabConfigure.h
    @brief Interface to the TabConfigure class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_TABCONFIGURE_H
#define LIBPF_LOC_TABCONFIGURE_H

class QLineEdit;
class QPushButton;
class QCheckBox;
class QSpinBox;
class QSlider;
class QFileDialog;
class QStatusBar;
class QIntValidator;

#include <QWidget>

#include "LibpfOpcConfigurations.h"

class TabConfigure : public QWidget {
  Q_OBJECT
  LibpfOPCConfigurations* m_Configs;
public:
  TabConfigure(QWidget *parent = 0);
  ~TabConfigure();
private:
  QLineEdit* m_pLineEditHomePath;
  QLineEdit* m_pLineEditKernelPath;
  QLineEdit* m_pLineEditCLSIDOfOPCServer;
  QLineEdit* m_pLineEditSleepTime;
  QLineEdit* m_pLineEditTraceServer;
  QLineEdit* m_pLineEditTracePipe;
  QPushButton* m_pButtonHomePath;
  QPushButton* m_pButtonKernelPath;

  QSpinBox* m_pSpinBoxGlobalVerbosity;
  QSpinBox* m_pSpinBoxFlashVerbosity;
  QSpinBox* m_pSpinBoxMaxErrorCount;
  QCheckBox* m_pCheckBoxReadOnlyMode;
  QFileDialog* m_pFileDialog;
  QSlider* m_pSliderSleepTime;

  void InitControls();
  void LoadConfig();
  void SaveConfig();

  QColor m_ColorActiveValid;
  QColor m_ColorInactiveValid;
  QColor m_ColorInvalid;
  bool IsFolderPathValid(const QString & sPath);
  QIntValidator* m_pSleepTimeValidator;
  void SetStateValid(QWidget* pTarget);
  void SetStateInactiveValid(QWidget* pTarget);
  void SetStateInvalid(QWidget* pTarget);

  QStatusBar* m_pStatusBar;
  QString m_sNoticeKey;
  QString m_sNoticeSetTo;
  QString m_sNoticeRestart;
  bool m_bIsLoading;

public slots:;
  void HandleButtonClickedHomePath(bool bIsChecked);
  void HandleButtonClickedKernelPath(bool bIsChecked);
  void HandleFolderTextChanged(const QString & text);
  void HandlerSleepTimeSliderChanged(int Value);
  void HandleSleepTimeTextChanged(const QString & sValue);
  void HandleFlashVebosityValueChanged(int iValue);
  void HandleGlobalVebosityValueChanged(int iValue);
  void HandleMaxErrorCountValueChanged(int iValue);
  void HandleReadOnlyModeValueChanged(int iValue);
  void HandleCLSIDServerValueChanged(const QString &);
  void HandleTracePipeValueChanged(const QString & sValue);
  void HandleTraceServerValueChanged(const QString & sValue);
  void HandleTraceSleedTimeEditingFinished();
  void HandleSleepTimeSliderReleased();
}; // class TabConfigure

#endif // LIBPF_LOC_TABCONFIGURE_H
