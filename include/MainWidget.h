/** @file MainWidget.h
    @brief Interface to the MainWidget class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_MAINWIDGET_H
#define LIBPF_LOC_MAINWIDGET_H

#include <QWidget>

#include "VariableGroup.h"
#include "TabVariables.h"

class MainWidget : public QWidget {
  Q_OBJECT
  TabVariables* m_pTabVariables;
public:
  MainWidget(QString & sAppPath, QWidget *parent = 0);
  ~MainWidget();
  /// Raise a dialog for confirming quitting if there is remain unsaved work
  /// @return true	If user confirmed quitting without saving current work.
  bool AskForQuit();
private:
  QTabWidget* m_pMainTab;
  QString m_sAppPath;
}; // class MainWidget

#endif // LIBPF_LOC_MAINWIDGET_H
