/** @file VariableInputDelegate.h
    @brief Interface to the VariableInputDelegate class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_VARIABLEINPUTDELEGATE_H
#define LIBPF_LOC_VARIABLEINPUTDELEGATE_H

#include <QItemDelegate>

class VariableInputDelegate :
  public QItemDelegate {
  Q_OBJECT
  QStringList m_listUOMValues;
public:
  VariableInputDelegate(QStringList ListUOMValues);
  ~VariableInputDelegate(void);
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;

  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,
                    const QModelIndex &index) const;

  void updateEditorGeometry(QWidget *editor,
                            const QStyleOptionViewItem &option, const QModelIndex &index) const;
}; // class VariableInputDelegate

#endif // LIBPF_LOC_VARIABLEINPUTDELEGATE_H
