/** @file Variable.h
    @brief Interface to the Variable class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_VARIABLE_H
#define LIBPF_LOC_VARIABLE_H

#include <QString>

class Variable {
public:
  enum VariableType {
    VT_INPUT,
    VT_RESULT,
    VT_CONSTANT
  };
  VariableType m_eType;
  QString m_sOPCtag;
  QString m_sDescription;
  QString m_sTag;
  QString m_sUOM;
  double m_dMin;
  double m_dMax;

public:
  static const double s_dUndefinedValue;
  Variable(void);
  ~Variable();
  Variable(const Variable &other);
  Variable &operator=(const Variable &other);
}; // class Variable

#endif // LIBPF_LOC_VARIABLE_H
