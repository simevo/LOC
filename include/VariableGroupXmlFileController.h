/** @file VariableGroupXmlFileController.h
    @brief Interface to the MessageHandler and VariableGroupXmlFileController class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef LIBPF_LOC_VARIABLEGROUPXMLFILECONTROLLER_H
#define LIBPF_LOC_VARIABLEGROUPXMLFILECONTROLLER_H

#include <QFile>
#include <QList>
#include <QString>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QtXmlPatterns>
#include <QObject>
#include <QStringList>

#include "VariableGroup.h"

// /////////////////////////////////////////////////////////////////////////////
// ////////////////////////////// MessageHandler  //////////////////////////////
// /////////////////////////////////////////////////////////////////////////////

class MessageHandler : public QAbstractMessageHandler {
public:
  MessageHandler()
    : QAbstractMessageHandler(0)
  {}

  QString statusMessage() const {
    QString message;
    switch (m_messageType) {
      case QtSystemMsg:
        message = "System validation error %1 at line %2 of settings.xml";
        break;
      case QtFatalMsg:
        message = "Fatal validation error %1 at line %2 of settings.xml";
        break;
      default:
        message = "Validation error %1 at line %2 of settings.xml";
        return m_description;
    }
    return message.arg(m_description).arg(m_sourceLocation.line());

  }

  int line() const {
    return m_sourceLocation.line();
  }

  int column() const {
    return m_sourceLocation.column();
  }

protected:
  virtual void handleMessage(QtMsgType type, const QString &description,
                             const QUrl &identifier, const QSourceLocation &sourceLocation) {
    Q_UNUSED(type);
    Q_UNUSED(identifier);

    m_messageType = type;
    m_description = description;
    m_sourceLocation = sourceLocation;
  }

private:
  QtMsgType m_messageType;
  QString m_description;
  QSourceLocation m_sourceLocation;
}; // class MessageHandler

// /////////////////////////////////////////////////////////////////////////////
// ////////////////////// VariableGroupXmlFileController  //////////////////////
// /////////////////////////////////////////////////////////////////////////////

class VariableGroupXmlFileController : public QObject {
private:
  Q_OBJECT VariableGroupXmlFileController(void);
  static VariableGroupXmlFileController* s_pSingletonObject;
public: ~VariableGroupXmlFileController(void);
  static VariableGroupXmlFileController* GetSingleton();
  static void ReleaseSingleton();

  bool LoadVariablesGroups(QList<VariableGroup>* pTarget, const QString &sFileName, const QString &sSchemaFileName, QString &sLastError);
  bool SaveVariablesGroups(QList<VariableGroup>* pTarget, const QString &sFileName, QString &sLastError);
  bool ValidateVariablesGroupsFile(const QString &sXmlFileName, const QString &sSchemaFileName, QString &sLastError);
  bool LoadUOMValuesList(const QString &sSchemaFileName, QStringList & ListValues, QString & sLastError);
private:

  void LoadGroup(QXmlStreamReader* pReader, VariableGroup* NewGroup);
  void LoadInput(QXmlStreamReader* pReader, VariableGroup* Owner);
  void LoadResult(QXmlStreamReader* pReader, VariableGroup* Owner);
  void LoadConstant(QXmlStreamReader* pReader, VariableGroup* Owner);
}; // class VariableGroupXmlFileController

#endif // LIBPF_LOC_VARIABLEGROUPXMLFILECONTROLLER_H
