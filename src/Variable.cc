/** @file Variable.cc
    @brief Implementation of the Variable class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "Variable.h"

const double Variable::s_dUndefinedValue = (double)((unsigned int)-1);

Variable::Variable(void) {
  m_eType = Variable::VT_INPUT;
  m_sOPCtag = "";
  m_sDescription = "";
  m_sTag = "";
  m_sUOM = "";
  m_dMin = s_dUndefinedValue;
  m_dMax = s_dUndefinedValue;
}

Variable::~Variable(void)
{}

Variable::Variable(const Variable &other) {
  m_eType = other.m_eType;
  m_sOPCtag = other.m_sOPCtag;
  m_sDescription = other.m_sDescription;
  m_sTag = other.m_sTag;
  m_sUOM = other.m_sUOM;
  m_dMin = other.m_dMin;
  m_dMax = other.m_dMax;
}

Variable &Variable::operator=(const Variable &other) {
  m_eType = other.m_eType;
  m_sOPCtag = other.m_sOPCtag;
  m_sDescription = other.m_sDescription;
  m_sTag = other.m_sTag;
  m_sUOM = other.m_sUOM;
  m_dMin = other.m_dMin;
  m_dMax = other.m_dMax;
  return *this;
}
