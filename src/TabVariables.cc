/** @file TabVariables.cc
    @brief Implementation of the TabVariables class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QHeaderView>
#include <QApplication>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QStandardItemModel>
#include <QListView>
#include <QMenu>
#include <QMessageBox>
#include <QMainWindow>
#include <QStringList>
#include <QTableWidget>
#include <QPushButton>
#include <QStringListModel>
#include <QTabWidget>
#include <QSplitter>
#include <QMenu>
#include <QComboBox>
#include <QStatusBar>

#include "VariableGroupXmlFileController.h"
#include "AppConfig.h"
#include "VariableInputDelegate.h"
#include "TabVariables.h"
#include "MySettings.h"

void formatTable(QTableWidget *table, const QStringList headers) {
  table->setColumnCount(headers.count());
  table->setHorizontalHeaderLabels(headers);

  QHeaderView *horizontalHeader = table->horizontalHeader();
  horizontalHeader->setVisible(true);
  horizontalHeader->sectionResizeMode(QHeaderView::ResizeToContents);
  horizontalHeader->setStyleSheet("QHeaderView { font-size: 12px; font-weight: bold; }");
  horizontalHeader->setCascadingSectionResizes(true);

  QHeaderView *verticalHeader = table->verticalHeader();
  verticalHeader->sectionResizeMode(QHeaderView::Fixed);
  verticalHeader->setDefaultSectionSize(24);
  verticalHeader->setVisible(true);

  // Behaviours
  table->setEditTriggers(QAbstractItemView::SelectedClicked);
  table->setSelectionBehavior(QAbstractItemView::SelectRows);
  table->setSelectionMode(QAbstractItemView::SingleSelection);
}

TabVariables::TabVariables(QWidget *parent)
  : QWidget(parent)
  , m_sUnsavedChanges(tr("Unsaved changes"))
  , m_FileDialog(this) {
  QString sLastError;
  MySettings settings;
  QString sSchemaFileName = settings.schemaFileName();
  if (!VariableGroupXmlFileController::GetSingleton()->LoadUOMValuesList(sSchemaFileName, this->m_listUOMValues, sLastError)) {
    this->m_listUOMValues = AppConfig::GetDefaultUOMValuesList();
    QMessageBox msgBox(this);
    msgBox.setText(tr("Failed to load schema file"));
    msgBox.setInformativeText(sLastError);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setWindowTitle(tr("LOC - non critical error"));
    msgBox.exec();
  }
  QMainWindow* pMainWindow = (QMainWindow*)parent->parent();
  this->m_pStatusBar = pMainWindow->statusBar();
  m_FileDialog.setAcceptMode(QFileDialog::AcceptOpen);
  m_FileDialog.setFileMode(QFileDialog::DirectoryOnly);

  // Load icons
  QPixmap pixAdd(":/LOC/images/add_icon.png");
  QPixmap pixRemove(":/LOC/images/delete_icon.png");
  m_iconAdd.addPixmap(pixAdd);
  m_iconRemove.addPixmap(pixRemove);

  // Setup table
  // Table of GroupVariables
  m_pTableVariableGroups = new QTableWidget(this);
  // Headers
  QStringList headers;
  headers << tr("Group id") << tr("Enabled") << tr("Description") << tr("Model");
  formatTable(m_pTableVariableGroups, headers);
  m_pTableVariableGroups->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
  // Layout
  // m_pTableVariableGroups->setBaseSize(500, 100);
  // m_pTableVariableGroups->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

  VariableInputDelegate *valDel = new VariableInputDelegate(this->m_listUOMValues);

  m_pTableVariableInput = new QTableWidget(this);
  m_pTableVariableInput->setItemDelegate(valDel);
  m_pTableVariableInput->setFocusPolicy(Qt::StrongFocus);
  QStringList InputHeaders;
  InputHeaders << tr("OPC tag") << tr("Description") << tr("LIBPF tag") << tr("Unit") << tr("Min") << tr("Max");
  formatTable(m_pTableVariableInput, InputHeaders);
  m_pTableVariableInput->setRowCount(0);
  m_pTableVariableInput->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

  m_pTableVariableResult = new QTableWidget(this);
  m_pTableVariableResult->setItemDelegate(valDel);
  m_pTableVariableResult->setFocusPolicy(Qt::StrongFocus);
  QStringList ResultHeaders;
  ResultHeaders << tr("OPC tag") << tr("Description") << tr("LIBPF tag") << tr("Unit");
  formatTable(m_pTableVariableResult, ResultHeaders);
  m_pTableVariableResult->setRowCount(0);
  m_pTableVariableResult->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

  m_pTableVariableConstant = new QTableWidget(this);
  QStringList ConstantHeaders;
  m_pTableVariableConstant->setItemDelegate(valDel);
  m_pTableVariableConstant->setFocusPolicy(Qt::StrongFocus);
  ConstantHeaders << tr("OPC tag") << tr("Description") << tr("LIBPF tag") << tr("Unit");
  formatTable(m_pTableVariableConstant, ConstantHeaders);
  m_pTableVariableConstant->setRowCount(0);
  m_pTableVariableConstant->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

  // Widget of variables
  m_pWidgetVariables = new QTabWidget(this);
  m_pWidgetVariables->setEnabled(false);
  m_pWidgetVariables->addTab(m_pTableVariableInput, tr("Inputs"));
  m_pWidgetVariables->addTab(m_pTableVariableResult, tr("Results"));
  m_pWidgetVariables->addTab(m_pTableVariableConstant, tr("Constants"));
  m_pWidgetVariables->setBaseSize(200, 200);
  m_pWidgetVariables->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

  // Handling events
  /*connect(m_pTableVariableGroups, SIGNAL(itemClicked(QTableWidgetItem*)),
      this, SLOT(HandleItemClicked(QTreeWidgetItem*)));*/
  connect(m_pTableVariableGroups, SIGNAL(currentItemChanged(QTableWidgetItem *, QTableWidgetItem*)),
          this, SLOT(HandleCurrentGroupChanged(QTableWidgetItem *, QTableWidgetItem*)));
  /*connect(m_pTableVariableGroups, SIGNAL(itemActivated (QTableWidgetItem*)),
      this, SLOT(HandleItemClicked(QTreeWidgetItem*)));*/
  connect(m_pTableVariableGroups, SIGNAL(itemSelectionChanged())
          , this, SLOT(HandleGroupSelectionChanged()));
  connect(m_pTableVariableGroups, SIGNAL(cellClicked(int,int))
          , this, SLOT(HandleVariableGroupTableCellClicked(int, int)));
  connect(m_pTableVariableGroups->verticalHeader(), SIGNAL(sectionPressed(int))
          , this, SLOT(HandleVariableGroupVerticalHeaderPressed(int)));
  connect(m_pTableVariableGroups, SIGNAL(cellChanged(int, int))
          , this, SLOT(HandleVariableGroupChanged(int, int)));

  connect(m_pTableVariableInput->verticalHeader(), SIGNAL(sectionPressed(int))
          , this, SLOT(HandleVariableInputVerticalHeaderPressed(int)));
  connect(m_pTableVariableInput, SIGNAL(cellChanged(int, int))
          , this, SLOT(HandleVariableInputChanged(int, int)));

  connect(m_pTableVariableResult->verticalHeader(), SIGNAL(sectionPressed(int))
          , this, SLOT(HandleVariableResultVerticalHeaderPressed(int)));
  connect(m_pTableVariableResult, SIGNAL(cellChanged(int, int))
          , this, SLOT(HandleVariableResultChanged(int, int)));

  connect(m_pTableVariableConstant->verticalHeader(), SIGNAL(sectionPressed(int))
          , this, SLOT(HandleVariableConstantVerticalHeaderPressed(int)));
  connect(m_pTableVariableConstant, SIGNAL(cellChanged(int, int))
          , this, SLOT(HandleVariableConstantChanged(int, int)));
  // Setup context menus
  QString strMoveUp(tr("Move up"));
  QString strMoveDown(tr("Move down"));
  // Variable group's context menu
  m_pContextMenuVariableGroup = new QMenu(this->m_pTableVariableGroups);
  m_pActionVariableGroupMoveUp = new QAction(strMoveUp, m_pContextMenuVariableGroup);
  connect(this->m_pActionVariableGroupMoveUp, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableGroupMoveUp()));
  m_pActionVariableGroupMoveDown = new QAction(strMoveDown, m_pContextMenuVariableGroup);
  connect(this->m_pActionVariableGroupMoveDown, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableGroupMoveDown()));
  m_pContextMenuVariableGroup->addAction(m_pActionVariableGroupMoveUp);
  m_pContextMenuVariableGroup->addAction(m_pActionVariableGroupMoveDown);
  m_pTableVariableGroups->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this->m_pTableVariableGroups, SIGNAL(customContextMenuRequested(const QPoint &))
          , this, SLOT(ActiveContextMenuVariableGroup(const QPoint &)));

  // Variable input context menu
  m_pContextMenuVariableInput = new QMenu(this->m_pTableVariableInput);
  m_pActionVariableInputMoveUp = new QAction(strMoveUp, m_pContextMenuVariableInput);
  connect(m_pActionVariableInputMoveUp, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableInputMoveUp()));
  m_pActionVariableInputMoveDown = new QAction(strMoveDown, m_pContextMenuVariableInput);
  connect(m_pActionVariableInputMoveDown, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableInputMoveDown()));
  m_pContextMenuVariableInput->addAction(m_pActionVariableInputMoveUp);
  m_pContextMenuVariableInput->addAction(m_pActionVariableInputMoveDown);
  m_pTableVariableInput->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this->m_pTableVariableInput, SIGNAL(customContextMenuRequested(const QPoint &))
          , this, SLOT(ActiveContextMenuVariableInput(const QPoint &)));

  // Variable result context menu
  m_pContextMenuVariableResult = new QMenu(this->m_pTableVariableResult);
  m_pActionVariableResultMoveUp = new QAction(strMoveUp, m_pContextMenuVariableResult);
  connect(m_pActionVariableResultMoveUp, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableResultMoveUp()));
  m_pActionVariableResultMoveDown = new QAction(strMoveDown, m_pContextMenuVariableResult);
  connect(m_pActionVariableResultMoveDown, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableResultMoveDown()));
  m_pContextMenuVariableResult->addAction(m_pActionVariableResultMoveUp);
  m_pContextMenuVariableResult->addAction(m_pActionVariableResultMoveDown);
  m_pTableVariableResult->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this->m_pTableVariableResult, SIGNAL(customContextMenuRequested(const QPoint &))
          , this, SLOT(ActiveContextMenuVariableResult(const QPoint &)));

  // Variable constant context menu
  m_pContextMenuVariableConstant = new QMenu(this->m_pTableVariableConstant);
  m_pActionVariableConstantMoveUp = new QAction(strMoveUp, m_pContextMenuVariableConstant);
  connect(m_pActionVariableConstantMoveUp, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableConstantMoveUp()));
  m_pActionVariableConstantMoveDown = new QAction(strMoveDown, m_pContextMenuVariableConstant);
  connect(m_pActionVariableConstantMoveDown, SIGNAL(triggered())
          , this, SLOT(HandleActionTriggeredVariableConstantMoveDown()));
  m_pContextMenuVariableConstant->addAction(m_pActionVariableConstantMoveUp);
  m_pContextMenuVariableConstant->addAction(m_pActionVariableConstantMoveDown);
  m_pTableVariableConstant->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this->m_pTableVariableConstant, SIGNAL(customContextMenuRequested(const QPoint &))
          , this, SLOT(ActiveContextMenuVariableConstant(const QPoint &)));

  m_pButtonImport = new QPushButton(tr("Import"), this);
  m_pButtonImport->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  m_pButtonExport = new QPushButton(tr("Export"), this);
  m_pButtonExport->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  m_pButtonRefresh = new QPushButton(tr("Refresh"), this);
  m_pButtonRefresh->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  m_pButtonSave = new QPushButton(tr("Save"), this);
  m_pButtonSave->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  this->m_pButtonSave->setEnabled(false);
  connect(m_pButtonSave, SIGNAL(clicked()),this, SLOT(HandleButtonClickedSave()));
  connect(m_pButtonImport, SIGNAL(clicked()),this, SLOT(HandleButtonClickedImport()));
  connect(m_pButtonExport, SIGNAL(clicked()),this, SLOT(HandleButtonClickedExport()));
  connect(m_pButtonRefresh, SIGNAL(clicked()),this, SLOT(HandleButtonClickedRefresh()));

  // MainLayout
  QGridLayout* pMainLayout = new QGridLayout();
  QHBoxLayout* pButtonLayout = new QHBoxLayout();
  pButtonLayout->addWidget(m_pButtonImport, Qt::AlignRight);
  pButtonLayout->addWidget(m_pButtonExport, Qt::AlignRight);
  pButtonLayout->addWidget(m_pButtonRefresh, Qt::AlignRight);
  pButtonLayout->addWidget(m_pButtonSave, Qt::AlignRight);

  m_pSplitter = new QSplitter(this);
  m_pSplitter->addWidget(m_pTableVariableGroups);
  m_pSplitter->addWidget(m_pWidgetVariables);
  pMainLayout->addLayout(pButtonLayout, 3, 1, Qt::AlignRight);
  // pMainLayout->addWidget(m_pTableVariableGroups, 0, 0);
  // pMainLayout->addWidget(m_pWidgetVariables, 0, 1);
  pMainLayout->addWidget(m_pSplitter, 0, 0, 1, 0);
  pMainLayout->setRowStretch(2, 0);
  m_pTableVariableGroups->resizeColumnsToContents();
  setLayout(pMainLayout);

  // Set drag and drop
  setAcceptDrops(true);
  this->m_bIsChanged = false;
  m_bIsLoadingData = false;
  m_bIsRowChanging = false;
  m_bIsRowMoving = false;
  m_bIsGroupChanging = false;
}

TabVariables::~TabVariables() {
  m_VariableGroups.clear();
}

void TabVariables::SetData(QList<VariableGroup>* VariableGroups) {
  qDebug() << "enter TabVariables::SetData";
  m_bIsLoadingData = true;
  m_bIsRowMoving = false;
  m_VariableGroups.clear();
  m_VariableGroups = *(VariableGroups);
  // int iTemp = m_VariableGroups.length();
  m_pTableVariableGroups->setRowCount(m_VariableGroups.length());
  for (int vi = 0; vi < VariableGroups->length(); vi++) {
    QTableWidgetItem *groupId = new QTableWidgetItem(VariableGroups->at(vi).m_sId);
    QTableWidgetItem *description = new QTableWidgetItem(VariableGroups->at(vi).m_sDescription);
    QTableWidgetItem *enabled = new QTableWidgetItem(true);
    if (VariableGroups->at(vi).m_bIsEnabled) {
      enabled->setCheckState(Qt::Checked);
    } else {
      enabled->setCheckState(Qt::Unchecked);
    }
    enabled->setFlags(Qt::NoItemFlags);
    enabled->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    QTableWidgetItem *model = new QTableWidgetItem(VariableGroups->at(vi).m_sModel);

    this->m_pTableVariableGroups->setItem(vi, 0, groupId);
    this->m_pTableVariableGroups->setItem(vi, 1, enabled);
    this->m_pTableVariableGroups->setItem(vi, 2, description);
    this->m_pTableVariableGroups->setItem(vi, 3, model);

    QTableWidgetItem *removeIcon = new QTableWidgetItem(0);
    removeIcon->setIcon(m_iconRemove);
    removeIcon->setFlags(Qt::NoItemFlags);
    removeIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    m_pTableVariableGroups->setVerticalHeaderItem(vi, removeIcon);
  }
  // Add button for adding new item
  AddNewRowPlaceHolder(m_pTableVariableGroups);
  m_pTableVariableGroups->resizeColumnsToContents();
  SetChangesMade(false);
  if (this->m_pTableVariableGroups->rowCount() > 1) {
    this->m_pTableVariableGroups->selectRow(0);
  } else {
    this->m_pWidgetVariables->setEnabled(false);
  }
  m_bIsLoadingData = false;
  qDebug() << "exit TabVariables::SetData";
}

void TabVariables::HandleItemClicked(QTableWidgetItem* item) {
  // int ret = 
  QMessageBox::warning(this, "TabVariables::HandleItemClicked",
                                 QString("row %1 clicked").arg(item->row()),
                                 QMessageBox::Save | QMessageBox::Discard
                                 | QMessageBox::Cancel,
                                 QMessageBox::Save);
} // TabVariables::HandleItemClicked

void TabVariables::HandleCurrentGroupChanged(QTableWidgetItem * current, QTableWidgetItem * /* previous */) {
  qDebug() << "enter TabVariables::HandleCurrentGroupChanged";
  if (current == NULL) {
    return;
  }
  m_bIsGroupChanging = true;
  QString groupId = this->m_pTableVariableGroups->item(this->m_pTableVariableGroups->currentItem()->row(), 0)->text();
  int groupIndex = -1;
  for (int vgi = 0; vgi < this->m_VariableGroups.length() && groupIndex == -1; vgi++) {
    if (this->m_VariableGroups.at(vgi).m_sId == groupId) {
      groupIndex = vgi;
    }
  }
  this->m_pTableVariableInput->clearContents();
  this->m_pTableVariableResult->clearContents();
  this->m_pTableVariableConstant->clearContents();
  if (groupIndex != -1) {
    this->SetWidgetVariableValue(this->m_VariableGroups.at(groupIndex));
  } else {
    // Not valid group
    this->m_pTableVariableInput->setRowCount(0);
    AddNewRowPlaceHolder(m_pTableVariableInput);
    this->m_pTableVariableInput->resizeColumnsToContents();
    this->m_pTableVariableInput->resizeRowsToContents();

    this->m_pTableVariableResult->setRowCount(0);
    AddNewRowPlaceHolder(m_pTableVariableResult);
    this->m_pTableVariableResult->resizeColumnsToContents();
    this->m_pTableVariableResult->resizeRowsToContents();

    this->m_pTableVariableConstant->setRowCount(0);
    AddNewRowPlaceHolder(m_pTableVariableConstant);
    this->m_pTableVariableConstant->resizeColumnsToContents();
    this->m_pTableVariableConstant->resizeRowsToContents();

    this->m_pWidgetVariables->setEnabled(true);
  }
  this->m_pWidgetVariables->setEnabled(true);
  m_bIsGroupChanging = false;
  qDebug() << "exit TabVariables::HandleCurrentGroupChanged";
}

void TabVariables::HandleButtonClickedSave() {
  // Validating step
  QString tmpXmlFile = QApplication::applicationDirPath();
  tmpXmlFile += "/";
  tmpXmlFile += AppConfig::s_sTempFileForValidating;

  MySettings settings;
  QString sSchemaFileName = settings.schemaFileName();

  QString sValidateError;
  if (VariableGroupXmlFileController::GetSingleton()->GetSingleton()->SaveVariablesGroups(
        &this->m_VariableGroups
        , tmpXmlFile
        , sValidateError)) {
    sValidateError = "";
    if (!VariableGroupXmlFileController::GetSingleton()->GetSingleton()->ValidateVariablesGroupsFile(tmpXmlFile, sSchemaFileName, sValidateError)) {
      QMessageBox msgBox(this);
      msgBox.setText(tr("Export file error"));
      msgBox.setInformativeText(sValidateError);
      msgBox.setStandardButtons(QMessageBox::Ok);
      msgBox.setDefaultButton(QMessageBox::Ok);
      msgBox.setIcon(QMessageBox::Warning);
      msgBox.setWindowTitle(tr("LOC - non critical error"));
      msgBox.exec();
      return;
    }
  }
  //QString temp = QApplication::applicationDirPath();
  // temp+= "\\data\\temp.xml";
  QString sLastError = tr("");
  if (VariableGroupXmlFileController::GetSingleton()->GetSingleton()->SaveVariablesGroups(
        &this->m_VariableGroups, m_sDefaultVariableGroupFile, sLastError)) {
    this->SetChangesMade(false);
  } else {
    QMessageBox msgBox(this);
    msgBox.setText(tr("Save file error"));
    msgBox.setInformativeText(sLastError);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setWindowTitle(tr("LOC - non critical error"));
    msgBox.exec();
  }
}

void TabVariables::HandleGroupSelectionChanged() {
  qDebug() << "enter TabVariables::HandleGroupSelectionChanged";
  m_bIsGroupChanging = true;

  // int length = this->m_pTableVariableGroups->selectedItems().length();
  if (this->m_pTableVariableGroups->selectedItems().length() > 0) {
    QString groupId = this->m_pTableVariableGroups->selectedItems().at(0)->text();
    int groupIndex = -1;
    for (int vgi = 0; vgi < this->m_VariableGroups.length() && groupIndex == -1; vgi++) {
      if (this->m_VariableGroups.at(vgi).m_sId == groupId) {
        groupIndex = vgi;
      }
    }
    this->m_pTableVariableInput->clearContents();
    this->m_pTableVariableResult->clearContents();
    this->m_pTableVariableConstant->clearContents();
    if (groupIndex != -1) {
      this->SetWidgetVariableValue(this->m_VariableGroups.at(groupIndex));
    } else {
      // Not valid group
      this->m_pTableVariableInput->setRowCount(0);
      AddNewRowPlaceHolder(m_pTableVariableInput);
      this->m_pTableVariableInput->resizeColumnsToContents();
      this->m_pTableVariableInput->resizeRowsToContents();

      this->m_pTableVariableResult->setRowCount(0);
      AddNewRowPlaceHolder(m_pTableVariableResult);
      this->m_pTableVariableResult->resizeColumnsToContents();
      this->m_pTableVariableResult->resizeRowsToContents();

      this->m_pTableVariableConstant->setRowCount(0);
      AddNewRowPlaceHolder(m_pTableVariableConstant);
      this->m_pTableVariableConstant->resizeColumnsToContents();
      this->m_pTableVariableConstant->resizeRowsToContents();

      this->m_pWidgetVariables->setEnabled(false);
    }
  }
  m_bIsGroupChanging = false;
  qDebug() << "exit TabVariables::HandleGroupSelectionChanged";
}

void TabVariables::closeEvent(QCloseEvent * /* event */) {
  // event->ignore();
}

bool TabVariables::AskForLeaveUnsavedWork(const QString message) {
  if (m_bIsChanged) {
    QMessageBox msgBox(this);
    msgBox.setText(tr("There are unsaved changes"));
    msgBox.setInformativeText(message);
    msgBox.addButton(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle(tr("LOC - warning"));
    msgBox.setDefaultButton(QMessageBox::No);
    if (msgBox.exec() == QMessageBox::No) {
      return false;
    }
  }
  return true;
}

void TabVariables::HandleButtonClickedImport() {
  if (!AskForLeaveUnsavedWork(tr("Are you sure you want to import an existing settings file ?"))) {
    return;
  }
  m_FileDialog.setAcceptMode(QFileDialog::AcceptOpen);
  m_FileDialog.setFileMode(QFileDialog::ExistingFile);
  if (m_FileDialog.exec()) {
    QStringList fileNames;
    fileNames = m_FileDialog.selectedFiles();
    if (fileNames.size() > 0) {
      QString sLastError;
      QList<VariableGroup>* pVariableGroups = new QList<VariableGroup>();
      MySettings settings;
      QString sSchemaFileName = settings.schemaFileName();
      if (VariableGroupXmlFileController::GetSingleton()->LoadVariablesGroups(pVariableGroups, fileNames[0], sSchemaFileName, sLastError)) {
        SetData(pVariableGroups);
      } else {
        QMessageBox msgBox(this);
        msgBox.setText("Error importing xml file");
        msgBox.setInformativeText(sLastError);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(tr("LOC - non critical error"));
        msgBox.exec();
      }
    }
  }
}

void TabVariables::HandleButtonClickedExport() {
  // Validating step
  QString tmpFile = QApplication::applicationDirPath();
  QString sValidateError;
  tmpFile += "/";
  tmpFile += AppConfig::s_sTempFileForValidating;
  if (VariableGroupXmlFileController::GetSingleton()->GetSingleton()->SaveVariablesGroups(
        &this->m_VariableGroups
        , tmpFile
        , sValidateError)) {
    sValidateError = "";
    MySettings settings;
    QString sSchemaFileName = settings.schemaFileName();
    if (!VariableGroupXmlFileController::GetSingleton()->GetSingleton()->ValidateVariablesGroupsFile(tmpFile, sSchemaFileName, sValidateError)) {
      QMessageBox msgBox(this);
      msgBox.setText(tr("Export file error"));
      msgBox.setInformativeText(sValidateError);
      msgBox.setStandardButtons(QMessageBox::Ok);
      msgBox.setDefaultButton(QMessageBox::Ok);
      msgBox.setIcon(QMessageBox::Warning);
      msgBox.setWindowTitle(tr("LOC - non critical error"));
      msgBox.exec();
      return;
    }
  }
  m_FileDialog.setAcceptMode(QFileDialog::AcceptSave);
  m_FileDialog.setFileMode(QFileDialog::AnyFile);
  if (m_FileDialog.exec()) {
    QStringList fileNames;
    fileNames = m_FileDialog.selectedFiles();
    if (fileNames.size() > 0) {
      QString sLastError;
      if (VariableGroupXmlFileController::GetSingleton()->SaveVariablesGroups(&this->m_VariableGroups, fileNames[0], sLastError)) {
        QMessageBox msgBox(this);
        msgBox.setText(tr("Exported!"));
        msgBox.setInformativeText(tr("settings file successfully exported!"));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowTitle(tr("LOC - info"));
        msgBox.exec();
      } else {
        QMessageBox msgBox(this);
        msgBox.setText(tr("Exporting failed!"));
        msgBox.setInformativeText(sLastError);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(tr("LOC - non critical error"));
        msgBox.exec();
      }
    }
  }
}

void TabVariables::HandleButtonClickedRefresh() {
  if (!AskForLeaveUnsavedWork(tr("Are you sure you want refresh to the view with the settings.xml file present on disk ?"))) {
    return;
  }
  this->m_pTableVariableGroups->clearContents();
  this->m_pTableVariableInput->clearContents();
  this->m_pTableVariableResult->clearContents();
  this->m_pTableVariableConstant->clearContents();
  LoadDefaultVariableGroupFile();
}

void TabVariables::dragEnterEvent(QDragEnterEvent * /* event */)
{}

void TabVariables::dragMoveEvent(QDragMoveEvent * /* event */)
{}

void TabVariables::dropEvent(QDropEvent * /* event */)
{}

void TabVariables::HandleVariableGroupTableCellClicked(int /* iRow */, int /* iColumn */) {
  // int row = iRow;
  // int col = iColumn;
  // int i = 0;
}

void TabVariables::HandleVariableGroupVerticalHeaderPressed(int iLogicalIndex) {
  if (iLogicalIndex == this->m_pTableVariableGroups->rowCount() - 1) {
    QTableWidgetItem *groupId = new QTableWidgetItem(tr(""));
    QTableWidgetItem *description = new QTableWidgetItem(tr(""));
    QTableWidgetItem *enabled = new QTableWidgetItem(tr(""));
    enabled->setCheckState(Qt::Unchecked);
    QTableWidgetItem *model = new QTableWidgetItem(tr(""));

    QTableWidgetItem *pRemoveIcon = new QTableWidgetItem(0);
    pRemoveIcon->setIcon(m_iconRemove);
    pRemoveIcon->setFlags(Qt::NoItemFlags);
    pRemoveIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    m_pTableVariableGroups->setVerticalHeaderItem(this->m_pTableVariableGroups->rowCount() - 1, pRemoveIcon);
    int iCurrentRow = this->m_pTableVariableGroups->rowCount() - 1;
    this->m_pTableVariableGroups->setItem(iCurrentRow, 0, groupId);
    this->m_pTableVariableGroups->setItem(iCurrentRow, 1, enabled);
    this->m_pTableVariableGroups->setItem(iCurrentRow, 2, description);
    this->m_pTableVariableGroups->setItem(iCurrentRow, 3, model);
    this->m_pTableVariableGroups->setVerticalHeaderItem(iCurrentRow, pRemoveIcon);
    this->m_pTableVariableGroups->selectRow(iCurrentRow);
    VariableGroup newGroup;
    this->m_VariableGroups.append(newGroup);
    AddNewRowPlaceHolder(m_pTableVariableGroups);
    SetChangesMade(true);
  } else {
    // Remove
    this->m_VariableGroups.removeAt(iLogicalIndex);
    this->m_pTableVariableInput->clearContents();
    this->m_pTableVariableResult->clearContents();
    this->m_pTableVariableConstant->clearContents();
    this->m_pTableVariableGroups->removeRow(iLogicalIndex);
    if (m_pTableVariableGroups->rowCount() > 0) {
      this->m_pTableVariableGroups->selectRow(iLogicalIndex - 1);
    }
    iLogicalIndex--;
    if (iLogicalIndex < 0) {
      iLogicalIndex = 0;
    }
    if (iLogicalIndex >= 0
        && iLogicalIndex < this->m_pTableVariableGroups->rowCount() - 1) {
      m_pTableVariableGroups->selectRow(iLogicalIndex);
      this->m_pWidgetVariables->setEnabled(true);
    } else {
      this->m_pTableVariableInput->clearContents();
      this->m_pTableVariableInput->setRowCount(0);
      this->m_pTableVariableResult->clearContents();
      this->m_pTableVariableResult->setRowCount(0);
      this->m_pTableVariableConstant->clearContents();
      this->m_pTableVariableConstant->setRowCount(0);
      this->m_pWidgetVariables->setEnabled(false);
    }
    SetChangesMade(true);
  }
}

void TabVariables::SetChangesMade(bool bIsChangesMade) {
  if (bIsChangesMade) {
    this->m_bIsChanged = true;
    this->m_pButtonSave->setEnabled(true);
    this->m_pButtonRefresh->setEnabled(true);
    this->m_pStatusBar->showMessage(m_sUnsavedChanges);
  } else {
    this->m_bIsChanged = false;
    this->m_pButtonSave->setEnabled(false);
    this->m_pButtonRefresh->setEnabled(false);
    this->m_pStatusBar->showMessage("");
  }
}

void TabVariables::SetWidgetVariableValue(const VariableGroup & CurrentVariableGroup) {
  this->m_CurrentVariableGroup = CurrentVariableGroup;
  QTableWidget* tw = NULL;
  int rowCount = 0;
  // Input variables
  tw = m_pTableVariableInput;
  tw->clearContents();
  rowCount = m_CurrentVariableGroup.m_listVariablesInput.length();
  tw->setRowCount(rowCount);
  for (int vi = 0; vi < rowCount; vi++) {
    const Variable* ptr = &m_CurrentVariableGroup.m_listVariablesInput.at(vi);
    QTableWidgetItem* OCPTag = new QTableWidgetItem(ptr->m_sOPCtag);
    QTableWidgetItem* description = new QTableWidgetItem(ptr->m_sDescription);
    QTableWidgetItem* libpfTag = new QTableWidgetItem(ptr->m_sTag);
    QTableWidgetItem* UOM = new QTableWidgetItem(ptr->m_sUOM);
    QTableWidgetItem* min = new QTableWidgetItem(QString(""));
    if (ptr->m_dMin != Variable::s_dUndefinedValue) {
      min->setText(QString::number(ptr->m_dMin));
    }
    QTableWidgetItem* max = new QTableWidgetItem(QString(""));
    if (ptr->m_dMin != Variable::s_dUndefinedValue) {
      min->setText(QString::number(ptr->m_dMin));
    }
    tw->setItem(vi, 0, OCPTag);
    tw->setItem(vi, 1, description);
    tw->setItem(vi, 2, libpfTag);
    tw->setItem(vi, 3, UOM);
    tw->setItem(vi, 4, min);
    tw->setItem(vi, 5, max);
    QTableWidgetItem *removeIcon = new QTableWidgetItem(0);
    removeIcon->setIcon(m_iconRemove);
    removeIcon->setFlags(Qt::NoItemFlags);
    removeIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    tw->setVerticalHeaderItem(vi, removeIcon);
  }
  tw->resizeColumnsToContents();
  tw->resizeRowsToContents();
  AddNewRowPlaceHolder(tw);
  // Result variable
  tw = m_pTableVariableResult;
  tw->clearContents();
  rowCount = m_CurrentVariableGroup.m_listVariablesResult.length();
  tw->setRowCount(rowCount);
  for (int vi = 0; vi < rowCount; vi++) {
    const Variable* ptr = &m_CurrentVariableGroup.m_listVariablesResult.at(vi);
    QTableWidgetItem* OCPTag = new QTableWidgetItem(ptr->m_sOPCtag);
    QTableWidgetItem* description = new QTableWidgetItem(ptr->m_sDescription);
    QTableWidgetItem* libpfTag = new QTableWidgetItem(ptr->m_sTag);
    QTableWidgetItem* UOM = new QTableWidgetItem(ptr->m_sUOM);
    tw->setItem(vi, 0, OCPTag);
    tw->setItem(vi, 1, description);
    tw->setItem(vi, 2, libpfTag);
    tw->setItem(vi, 3, UOM);
    QTableWidgetItem *removeIcon = new QTableWidgetItem(0);
    removeIcon->setIcon(m_iconRemove);
    removeIcon->setFlags(Qt::NoItemFlags);
    removeIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    tw->setVerticalHeaderItem(vi, removeIcon);
  }
  tw->resizeColumnsToContents();
  tw->resizeRowsToContents();
  AddNewRowPlaceHolder(tw);
  // Constant variable
  tw = m_pTableVariableConstant;
  tw->clearContents();
  rowCount = m_CurrentVariableGroup.m_listVariablesConstant.length();
  tw->setRowCount(rowCount);
  for (int vi = 0; vi < rowCount; vi++) {
    const Variable* ptr = &m_CurrentVariableGroup.m_listVariablesConstant.at(vi);
    QTableWidgetItem* OCPTag = new QTableWidgetItem(ptr->m_sOPCtag);
    QTableWidgetItem* description = new QTableWidgetItem(ptr->m_sDescription);
    QTableWidgetItem* libpfTag = new QTableWidgetItem(ptr->m_sTag);
    QTableWidgetItem* UOM = new QTableWidgetItem(ptr->m_sUOM);
    tw->setItem(vi, 0, OCPTag);
    tw->setItem(vi, 1, description);
    tw->setItem(vi, 2, libpfTag);
    tw->setItem(vi, 3, UOM);
    QTableWidgetItem *removeIcon = new QTableWidgetItem(0);
    removeIcon->setIcon(m_iconRemove);
    removeIcon->setFlags(Qt::NoItemFlags);
    removeIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    tw->setVerticalHeaderItem(vi, removeIcon);
  }
  tw->resizeColumnsToContents();
  tw->resizeRowsToContents();
  AddNewRowPlaceHolder(tw);
  this->m_pWidgetVariables->setEnabled(true);
}

void TabVariables::HandleVariableInputVerticalHeaderPressed(int iLogicalIndex) {
  if (iLogicalIndex == this->m_pTableVariableInput->rowCount() - 1) {
    // Add new row
    AddNewRowToVariableTable(m_pTableVariableInput);
  } else {
    // Remove
    RemoveARowFromVariableTable(m_pTableVariableInput, iLogicalIndex);
  }
}

void TabVariables::HandleVariableResultVerticalHeaderPressed(int iLogicalIndex) {
  if (iLogicalIndex == this->m_pTableVariableResult->rowCount() - 1) {
    // Add new row
    AddNewRowToVariableTable(m_pTableVariableResult);
  } else {
    // Remove
    RemoveARowFromVariableTable(m_pTableVariableResult, iLogicalIndex);
  }
}

void TabVariables::HandleVariableConstantVerticalHeaderPressed(int iLogicalIndex) {
  if (iLogicalIndex == this->m_pTableVariableConstant->rowCount() - 1) {
    // Add new row
    AddNewRowToVariableTable(m_pTableVariableConstant);
  } else {
    // Remove
    RemoveARowFromVariableTable(m_pTableVariableConstant, iLogicalIndex);
  }
}

void TabVariables::AddNewRowPlaceHolder(QTableWidget* pTableVariable) {
  m_bIsRowChanging = true;
  QTableWidgetItem *pAddIcon = new QTableWidgetItem();
  int iNewRowCount = pTableVariable->rowCount() + 1;
  pTableVariable->setRowCount(iNewRowCount);

  pAddIcon->setIcon(m_iconAdd);
  pAddIcon->setFlags(Qt::NoItemFlags);
  pAddIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  // int colCount = pTableVariable->horizontalHeader()->count();
  for (int i = 0; i < pTableVariable->horizontalHeader()->count(); i++) {
    QTableWidgetItem* placeHolderItem = new QTableWidgetItem();
    placeHolderItem->setFlags(Qt::NoItemFlags);
    pTableVariable->setItem(iNewRowCount - 1, i, placeHolderItem);
  }
  pTableVariable->setVerticalHeaderItem(iNewRowCount - 1, pAddIcon);
  m_bIsRowChanging = false;
}

void TabVariables::RemoveARowFromVariableTable(QTableWidget* pTableVariable, int iIndex) {
  m_bIsRowChanging = true;
  if (iIndex >= 0
      && iIndex < pTableVariable->rowCount()) {
    pTableVariable->removeRow(iIndex);
    if (this->m_pTableVariableGroups->selectedItems().length() > 0) {
      int iGroupIndex = this->m_pTableVariableGroups->selectedItems().at(0)->row();
      if (iGroupIndex >= this->m_VariableGroups.length()) {
        return;
      }
      if (pTableVariable == this->m_pTableVariableGroups) {
        this->m_VariableGroups.removeAt(iGroupIndex);
        SetChangesMade(true);
      } else {
        if (pTableVariable == this->m_pTableVariableInput) {
          if (iIndex >= this->m_VariableGroups[iGroupIndex].m_listVariablesInput.length()) {
            return;
          }
          this->m_VariableGroups[iGroupIndex].m_listVariablesInput.removeAt(iIndex);
          SetChangesMade(true);
        } else if (pTableVariable == this->m_pTableVariableResult) {
          if (iIndex >= this->m_VariableGroups[iGroupIndex].m_listVariablesResult.length()) {
            return;
          }
          this->m_VariableGroups[iGroupIndex].m_listVariablesResult.removeAt(iIndex);
          SetChangesMade(true);
        } else if (pTableVariable == this->m_pTableVariableConstant) {
          if (iIndex >= this->m_VariableGroups[iGroupIndex].m_listVariablesConstant.length()) {
            return;
          }
          this->m_VariableGroups[iGroupIndex].m_listVariablesConstant.removeAt(iIndex);
          SetChangesMade(true);
        }
      }
    }
  }
  m_bIsRowChanging = false;
}

void TabVariables::AddNewRowToVariableTable(QTableWidget* pTableVariable) {
  m_bIsRowChanging = true;
  int iGroupIndex = -1;
  if (this->m_pTableVariableGroups->selectedItems().length() > 0) {
    if (this->m_pTableVariableGroups->selectedItems().at(0)->row() >= 0
        && this->m_pTableVariableGroups->selectedItems().at(0)->row() < this->m_VariableGroups.length()) {
      iGroupIndex = this->m_pTableVariableGroups->selectedItems().at(0)->row();
    }
  }
  if (iGroupIndex == -1) {
    return;
  }
  QTableWidgetItem *pRemoveIcon = new QTableWidgetItem();
  pRemoveIcon->setIcon(m_iconRemove);
  pRemoveIcon->setFlags(Qt::NoItemFlags);
  pRemoveIcon->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  pTableVariable->setVerticalHeaderItem(pTableVariable->rowCount() - 1, pRemoveIcon);
  int iCurrentRow = 0;
  int iRowCount = pTableVariable->rowCount();
  int iColCount = pTableVariable->columnCount();
  if (iRowCount > 0) {
    iCurrentRow = pTableVariable->rowCount() - 1;
  }
  for (int i = 0; i < iColCount; i++) {
    if (i == 3) {
      QComboBox *pUOM = new QComboBox();
      pUOM->setAcceptDrops(true);
      pUOM->addItems(m_listUOMValues);
      pUOM->setCurrentIndex(0);
      if (pTableVariable == this->m_pTableVariableInput) {
        connect(pUOM, SIGNAL(currentIndexChanged(const QString &))
                , this, SLOT(HandleInputVariableUOMValueChanged(const QString &)));
      } else if (pTableVariable == this->m_pTableVariableResult) {
        connect(pUOM, SIGNAL(currentIndexChanged(const QString &))
                , this, SLOT(HandleResultVariableUOMValueChanged(const QString &)));
      }
      if (pTableVariable == this->m_pTableVariableConstant) {
        connect(pUOM, SIGNAL(currentIndexChanged(const QString &))
                , this, SLOT(HandleConstantVariableUOMValueChanged(const QString &)));
      }
      pTableVariable->setCellWidget(iCurrentRow, i, pUOM);
    } else {
      QTableWidgetItem *item = new QTableWidgetItem(tr(""));
      pTableVariable->setItem(iCurrentRow, i, item);
    }
  }
  pTableVariable->setVerticalHeaderItem(iCurrentRow, pRemoveIcon);
  // Add new variable
  Variable newVal;
  if (pTableVariable == this->m_pTableVariableInput) {
    this->m_VariableGroups[iGroupIndex].m_listVariablesInput.append(newVal);
  } else if (pTableVariable == this->m_pTableVariableResult) {
    this->m_VariableGroups[iGroupIndex].m_listVariablesResult.append(newVal);
  } else if (pTableVariable == this->m_pTableVariableConstant) {
    this->m_VariableGroups[iGroupIndex].m_listVariablesConstant.append(newVal);
  }
  AddNewRowPlaceHolder(pTableVariable);
  SetChangesMade(true);
  m_bIsRowChanging = false;
}

void TabVariables::HandleVariableGroupChanged(int iRow, int /* iColumn */) {
  qDebug() << "changed row " << iRow << " of variable group table";
  HandleCellChanged(m_pTableVariableGroups, iRow);
}

void TabVariables::HandleVariableInputChanged(int iRow, int /* iColumn */) {
  qDebug() << "changed row " << iRow << " of input variables table";
  HandleCellChanged(m_pTableVariableInput, iRow);
}

void TabVariables::HandleVariableResultChanged(int iRow, int /* iColumn */) {
  qDebug() << "changed row " << iRow << " of result variables table";
  HandleCellChanged(m_pTableVariableResult, iRow);
}

void TabVariables::HandleVariableConstantChanged(int iRow, int /* iColumn */) {
  qDebug() << "changed row " << iRow << " of constant variables table";
  HandleCellChanged(m_pTableVariableConstant, iRow);
}

void TabVariables::HandleCellChanged(QTableWidget* table, int iRow) {
  if (!m_bIsLoadingData && !m_bIsRowChanging && !m_bIsGroupChanging) {
    UpdateValueChanged(table, iRow);
    SetChangesMade(true);
  }
}

void TabVariables::ActiveContextMenuVariableGroup(const QPoint &pos) {
  if (this->m_pTableVariableGroups->selectedItems().length() <= 0) {
    return;
  }
  this->m_pTableVariableGroups->selectedItems().at(0)->row() == 0
  ? this->m_pActionVariableGroupMoveUp->setEnabled(false)
  : this->m_pActionVariableGroupMoveUp->setEnabled(true);

  this->m_pTableVariableGroups->selectedItems().at(0)->row() == this->m_pTableVariableGroups->rowCount() - 2
  ? this->m_pActionVariableGroupMoveDown->setEnabled(false)
  : this->m_pActionVariableGroupMoveDown->setEnabled(true);
  this->m_pContextMenuVariableGroup->exec(this->m_pTableVariableGroups->mapToGlobal(pos));
}

void TabVariables::ActiveContextMenuVariableInput(const QPoint &pos) {
  if (this->m_pTableVariableInput->selectedItems().length() <= 0) {
    return;
  }
  this->m_pTableVariableInput->selectedItems().at(0)->row() == 0
  ? this->m_pActionVariableInputMoveUp->setEnabled(false)
  : this->m_pActionVariableInputMoveUp->setEnabled(true);

  this->m_pTableVariableInput->selectedItems().at(0)->row() == this->m_pTableVariableInput->rowCount() - 2
  ? this->m_pActionVariableInputMoveDown->setEnabled(false)
  : this->m_pActionVariableInputMoveDown->setEnabled(true);
  this->m_pContextMenuVariableInput->exec(this->m_pTableVariableInput->mapToGlobal(pos));
}

void TabVariables::ActiveContextMenuVariableResult(const QPoint &pos) {
  if (this->m_pTableVariableResult->selectedItems().length() <= 0) {
    return;
  }
  this->m_pTableVariableResult->selectedItems().at(0)->row() == 0
  ? this->m_pActionVariableResultMoveUp->setEnabled(false)
  : this->m_pActionVariableResultMoveUp->setEnabled(true);

  this->m_pTableVariableResult->selectedItems().at(0)->row() == this->m_pTableVariableResult->rowCount() - 2
  ? this->m_pActionVariableResultMoveDown->setEnabled(false)
  : this->m_pActionVariableResultMoveDown->setEnabled(true);
  this->m_pContextMenuVariableResult->exec(this->m_pTableVariableResult->mapToGlobal(pos));
}

void TabVariables::ActiveContextMenuVariableConstant(const QPoint &pos) {
  if (this->m_pTableVariableConstant->selectedItems().length() <= 0) {
    return;
  }
  this->m_pTableVariableConstant->selectedItems().at(0)->row() == 0
  ? this->m_pActionVariableConstantMoveUp->setEnabled(false)
  : this->m_pActionVariableConstantMoveUp->setEnabled(true);

  this->m_pTableVariableConstant->selectedItems().at(0)->row() == this->m_pTableVariableConstant->rowCount() - 2
  ? this->m_pActionVariableConstantMoveDown->setEnabled(false)
  : this->m_pActionVariableConstantMoveDown->setEnabled(true);
  this->m_pContextMenuVariableConstant->exec(this->m_pTableVariableConstant->mapToGlobal(pos));
}

void TabVariables::HandleActionTriggeredVariableGroupMoveUp() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  m_bIsRowMoving = true;

  int iNewIndex = iCurrentGroup - 1;
  MoveTableRow(this->m_pTableVariableGroups, iCurrentGroup, iNewIndex);
  MoveVariableGroup(this->m_VariableGroups, iCurrentGroup, iNewIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableGroups->selectRow(iNewIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableGroupMoveDown() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  m_bIsRowMoving = true;

  int iNewIndex = iCurrentGroup + 1;
  MoveTableRow(this->m_pTableVariableGroups, iCurrentGroup, iNewIndex);
  MoveVariableGroup(this->m_VariableGroups, iCurrentGroup, iNewIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableGroups->selectRow(iNewIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableInputMoveUp() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  int iCurrentInputIndex = this->GetSelectingIndexOnWidget(this->m_pTableVariableInput);
  if (iCurrentInputIndex == -1) {
    return;
  }
  m_bIsRowMoving = true;
  int iNewInputIndex = iCurrentInputIndex - 1;

  MoveTableRow(this->m_pTableVariableInput, iCurrentInputIndex, iNewInputIndex);
  MoveVariable(this->m_VariableGroups[iCurrentGroup].m_listVariablesInput, iCurrentInputIndex, iNewInputIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableInput->selectRow(iNewInputIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableInputMoveDown() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  int iCurrentInputIndex = this->GetSelectingIndexOnWidget(this->m_pTableVariableInput);
  if (iCurrentInputIndex == -1) {
    return;
  }
  m_bIsRowMoving = true;
  int iNewInputIndex = iCurrentInputIndex + 1;

  MoveTableRow(this->m_pTableVariableInput, iCurrentInputIndex, iNewInputIndex);
  MoveVariable(this->m_VariableGroups[iCurrentGroup].m_listVariablesInput, iCurrentInputIndex, iNewInputIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableInput->selectRow(iNewInputIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableResultMoveUp() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  int iCurrentResultIndex = this->GetSelectingIndexOnWidget(this->m_pTableVariableResult);
  if (iCurrentResultIndex == -1) {
    return;
  }
  m_bIsRowMoving = true;
  int iNewResultIndex = iCurrentResultIndex - 1;

  MoveTableRow(this->m_pTableVariableResult, iCurrentResultIndex, iNewResultIndex);
  MoveVariable(this->m_VariableGroups[iCurrentGroup].m_listVariablesResult, iCurrentResultIndex, iNewResultIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableResult->selectRow(iNewResultIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableResultMoveDown() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  int iCurrentResultIndex = this->GetSelectingIndexOnWidget(this->m_pTableVariableResult);
  if (iCurrentResultIndex == +1) {
    return;
  }
  m_bIsRowMoving = true;
  int iNewResultIndex = iCurrentResultIndex - 1;

  MoveTableRow(this->m_pTableVariableResult, iCurrentResultIndex, iNewResultIndex);
  MoveVariable(this->m_VariableGroups[iCurrentGroup].m_listVariablesResult, iCurrentResultIndex, iNewResultIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableResult->selectRow(iNewResultIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableConstantMoveUp() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  int iCurrentConstantIndex = this->GetSelectingIndexOnWidget(this->m_pTableVariableConstant);
  if (iCurrentConstantIndex == -1) {
    return;
  }
  m_bIsRowMoving = true;
  int iNewConstantIndex = iCurrentConstantIndex - 1;

  MoveTableRow(this->m_pTableVariableConstant, iCurrentConstantIndex, iNewConstantIndex);
  MoveVariable(this->m_VariableGroups[iCurrentGroup].m_listVariablesConstant, iCurrentConstantIndex, iNewConstantIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableConstant->selectRow(iNewConstantIndex);
  this->SetChangesMade(true);
}

void TabVariables::HandleActionTriggeredVariableConstantMoveDown() {
  int iCurrentGroup = this->GetSelectingIndexOnWidget(this->m_pTableVariableGroups);
  if (iCurrentGroup == -1) {
    return;
  }
  int iCurrentConstantIndex = this->GetSelectingIndexOnWidget(this->m_pTableVariableConstant);
  if (iCurrentConstantIndex == -1) {
    return;
  }
  m_bIsRowMoving = true;
  int iNewConstantIndex = iCurrentConstantIndex + 1;

  MoveTableRow(this->m_pTableVariableConstant, iCurrentConstantIndex, iNewConstantIndex);
  MoveVariable(this->m_VariableGroups[iCurrentGroup].m_listVariablesConstant, iCurrentConstantIndex, iNewConstantIndex);
  m_bIsRowMoving = false;
  this->m_pTableVariableConstant->selectRow(iNewConstantIndex);
  this->SetChangesMade(true);
}

void TabVariables::MoveTableRow(QTableWidget* pTable, int iSrcIndex, int iDesIndex) {
  bool isIndexesValid = iSrcIndex <= 0
                        && iSrcIndex >= pTable->rowCount() - 2
                        && iDesIndex <= 0
                        && iDesIndex >= pTable->rowCount() - 2;
  if (pTable == NULL
      || pTable->selectedItems().length() <= 0
      || isIndexesValid) {
    return;
  }
  QList<QTableWidgetItem*> srcRowItems;
  for (int ci = 0; ci < pTable->columnCount(); ci++) {
    srcRowItems.append(pTable->takeItem(iSrcIndex, ci));
  }
  QList<QTableWidgetItem*> desRowItems;
  for (int ci = 0; ci < pTable->columnCount(); ci++) {
    desRowItems.append(pTable->takeItem(iDesIndex, ci));
  }
  for (int ci = 0; ci < srcRowItems.length(); ci++) {
    pTable->setItem(iDesIndex, ci, srcRowItems[ci]);
  }
  for (int ci = 0; ci < desRowItems.length(); ci++) {
    pTable->setItem(iSrcIndex, ci, desRowItems[ci]);
  }
}

void TabVariables::SetDefaultVariableGroupFile(const QString & sFileName) {
  m_sDefaultVariableGroupFile = sFileName;
}

void TabVariables::LoadDefaultVariableGroupFile() {
  QString sLastError;
  QList<VariableGroup>* pVariableGroups = new QList<VariableGroup>();
  MySettings settings;
  QString sSchemaFileName = settings.schemaFileName();
  if (VariableGroupXmlFileController::GetSingleton()->LoadVariablesGroups(pVariableGroups, this->m_sDefaultVariableGroupFile, sSchemaFileName, sLastError)) {} else {
    QMessageBox msgBox(this);
    msgBox.setText(tr("Error loading xml file"));
    msgBox.setInformativeText(sLastError);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setFixedWidth(1000);
    msgBox.setWindowTitle(tr("LOC - non critical error"));
    msgBox.exec();
  }
  this->SetData(pVariableGroups);
}

void TabVariables::UpdateValueChanged(QTableWidget* pTable, int iRowIndex) {
  if (m_bIsRowMoving) {
    return;
  }
  if (pTable == m_pTableVariableGroups) {
    bool indexValid = iRowIndex >= 0
                      && iRowIndex < this->m_VariableGroups.length();
    if (indexValid) {
      this->m_VariableGroups[iRowIndex].m_sId = this->m_pTableVariableGroups->item(iRowIndex, 0)->text();
      this->m_VariableGroups[iRowIndex].m_sDescription = this->m_pTableVariableGroups->item(iRowIndex, 2)->text();
      if (this->m_pTableVariableGroups->item(iRowIndex, 1)->checkState() == Qt::Checked) {
        this->m_VariableGroups[iRowIndex].m_bIsEnabled = true;
      } else {
        this->m_VariableGroups[iRowIndex].m_bIsEnabled = false;
      }
      this->m_VariableGroups[iRowIndex].m_sModel = this->m_pTableVariableGroups->item(iRowIndex, 3)->text();
      return;
    } else {
      return;
    }
  }
  // Get selecting variable group
  if (this->m_pTableVariableGroups->selectedItems().length() <= 0) {
    return;
  }
  int iGroupIndex = this->m_pTableVariableGroups->selectedItems().at(0)->row();
  if (iGroupIndex >= this->m_VariableGroups.length()) {
    return;
  }
  if (pTable == this->m_pTableVariableInput) {
    if (iRowIndex < this->m_VariableGroups[iGroupIndex].m_listVariablesInput.length()) {
      VariableGroup tempGroup = this->m_VariableGroups.at(iGroupIndex);
      this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_sOPCtag = pTable->item(iRowIndex, 0)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_sDescription = pTable->item(iRowIndex, 1)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_sTag = pTable->item(iRowIndex, 2)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_sUOM = pTable->item(iRowIndex, 3)->text();

      if (pTable->item(iRowIndex, 4)->text() != "") {
        this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_dMin = pTable->item(iRowIndex, 4)->text().toDouble();
        pTable->item(iRowIndex, 4)->setText(QString::number(this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_dMin));
      } else {
        this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_dMin = Variable::s_dUndefinedValue;
      }
      if (pTable->item(iRowIndex, 5)->text() != "") {
        this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_dMax = pTable->item(iRowIndex, 5)->text().toDouble();
        pTable->item(iRowIndex, 5)->setText(QString::number(this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_dMax));
      } else {
        this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iRowIndex].m_dMax = Variable::s_dUndefinedValue;
      }
    }
  } else if (pTable == this->m_pTableVariableResult) {
    if (iRowIndex < this->m_VariableGroups[iGroupIndex].m_listVariablesResult.length()) {
      this->m_VariableGroups[iGroupIndex].m_listVariablesResult[iRowIndex].m_sOPCtag = pTable->item(iRowIndex, 0)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesResult[iRowIndex].m_sDescription = pTable->item(iRowIndex, 1)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesResult[iRowIndex].m_sTag = pTable->item(iRowIndex, 2)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesResult[iRowIndex].m_sUOM = pTable->item(iRowIndex, 3)->text();
    }
  } else if (pTable == this->m_pTableVariableConstant) {
    if (iRowIndex < this->m_VariableGroups[iGroupIndex].m_listVariablesConstant.length()) {
      this->m_VariableGroups[iGroupIndex].m_listVariablesConstant[iRowIndex].m_sOPCtag = pTable->item(iRowIndex, 0)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesConstant[iRowIndex].m_sDescription = pTable->item(iRowIndex, 1)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesConstant[iRowIndex].m_sTag = pTable->item(iRowIndex, 2)->text();
      this->m_VariableGroups[iGroupIndex].m_listVariablesConstant[iRowIndex].m_sUOM = pTable->item(iRowIndex, 3)->text();
    }
  }
}

void TabVariables::HandleInputVariableUOMValueChanged(const QString & text) {
  QComboBox* sender = (QComboBox*)QObject::sender();
  for (int i = 0; i < this->m_pTableVariableInput->rowCount(); i++) {
    if (sender == m_pTableVariableInput->cellWidget(i, 3)) {
      this->m_pTableVariableInput->selectRow(i);
      // m_pTableVariableInput->setCurrentCell(i,4);
      break;
    }
  }
  if (this->m_pTableVariableGroups->selectedItems().length() <= 0) {
    return;
  }
  if (this->m_pTableVariableInput->selectedItems().length() <= 0) {
    return;
  }
  int iGroupIndex = this->m_pTableVariableGroups->selectedItems().at(0)->row();
  if (iGroupIndex < 0 || iGroupIndex >= this->m_VariableGroups.length()) {
    return;
  }
  int iInputIndex = this->m_pTableVariableInput->selectedItems().at(0)->row();
  if (iInputIndex < 0 || iInputIndex >= this->m_VariableGroups[iGroupIndex].m_listVariablesInput.length()) {
    return;
  }
  this->m_VariableGroups[iGroupIndex].m_listVariablesInput[iInputIndex].m_sUOM = text;

  this->SetChangesMade(true);
}

void TabVariables::HandleResultVariableUOMValueChanged(const QString & text) {
  QObject* sender = QObject::sender();
  for (int i = 0; i < this->m_pTableVariableResult->rowCount(); i++) {
    if (sender == m_pTableVariableResult->cellWidget(i, 3)) {
      this->m_pTableVariableResult->selectRow(i);
      break;
    }
  }
  if (this->m_pTableVariableGroups->selectedItems().length() <= 0) {
    return;
  }
  if (this->m_pTableVariableResult->selectedItems().length() <= 0) {
    return;
  }
  int iGroupIndex = this->m_pTableVariableGroups->selectedItems().at(0)->row();
  if (iGroupIndex < 0 || iGroupIndex >= this->m_VariableGroups.length()) {
    return;
  }
  int iResultIndex = this->m_pTableVariableResult->selectedItems().at(0)->row();
  if (iResultIndex < 0 || iResultIndex >= this->m_VariableGroups[iGroupIndex].m_listVariablesResult.length()) {
    return;
  }
  this->m_VariableGroups[iGroupIndex].m_listVariablesResult[iResultIndex].m_sUOM = text;
  this->SetChangesMade(true);
}

void TabVariables::HandleConstantVariableUOMValueChanged(const QString & text) {
  QObject* sender = QObject::sender();
  for (int i = 0; i < this->m_pTableVariableConstant->rowCount(); i++) {
    if (sender == m_pTableVariableConstant->cellWidget(i, 3)) {
      this->m_pTableVariableConstant->selectRow(i);
      break;
    }
  }
  if (this->m_pTableVariableGroups->selectedItems().length() <= 0) {
    return;
  }
  if (this->m_pTableVariableConstant->selectedItems().length() <= 0) {
    return;
  }
  int iGroupIndex = this->m_pTableVariableGroups->selectedItems().at(0)->row();
  if (iGroupIndex < 0 || iGroupIndex >= this->m_VariableGroups.length()) {
    return;
  }
  int iConstantIndex = this->m_pTableVariableConstant->selectedItems().at(0)->row();
  if (iConstantIndex < 0 || iConstantIndex >= this->m_VariableGroups[iGroupIndex].m_listVariablesConstant.length()) {
    return;
  }
  this->m_VariableGroups[iGroupIndex].m_listVariablesConstant[iConstantIndex].m_sUOM = text;
  this->SetChangesMade(true);
}

void TabVariables::MoveVariableGroup(QList<VariableGroup> & Target, int iSrcIndex, int iDesIndex) {
  if (iSrcIndex < 0
      || iSrcIndex >= Target.length()
      || iDesIndex < 0
      || iDesIndex >= Target.length()) {
    return;
  }
  VariableGroup temp = Target[iSrcIndex];
  Target[iSrcIndex] = Target[iDesIndex];
  Target[iDesIndex] = temp;
  // int i = Target.length();
}

void TabVariables::MoveVariable(QList<Variable> & Target, int iSrcIndex, int iDesIndex) {
  if (iSrcIndex < 0
      || iSrcIndex >= Target.length()
      || iDesIndex < 0
      || iDesIndex >= Target.length()) {
    return;
  }
  std::swap(Target[iSrcIndex], Target[iDesIndex]);
}

int TabVariables::GetSelectingIndexOnWidget(QTableWidget* pTarget) {
  if (pTarget->selectedItems().length() > 0) {
    return pTarget->selectedItems().at(0)->row();
  }
  return -1;
}
