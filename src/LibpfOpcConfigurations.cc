/** @file LibpfOpcConfigurations.cc
    @brief Implementation of the LibpfOPCConfigurations class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "LibpfOpcConfigurations.h"

LibpfOPCConfigurations::LibpfOPCConfigurations(void) {
  m_sHomePath = "";
  m_sKernelPath = "";
  m_sCLSIDOfOPCServer = "";
  m_iSleepTime = 0;
  m_sTracePipe = "";
  m_sTraceServer = "";
  m_iGlobalVerbosity = 0;
  m_iFlashVerbosity = 0;
  m_iReadOnlyMode = 0;
  m_iMaxErrorCount = 0;
}

LibpfOPCConfigurations::~LibpfOPCConfigurations(void)
{}

LibpfOPCConfigurations::LibpfOPCConfigurations(const LibpfOPCConfigurations & other) {
  m_sHomePath = other.m_sHomePath;
  m_sKernelPath = other.m_sKernelPath;
  m_sCLSIDOfOPCServer = other.m_sCLSIDOfOPCServer;
  m_iSleepTime = other.m_iSleepTime;
  m_sTracePipe = other.m_sTracePipe;
  m_sTraceServer = other.m_sTraceServer;
  m_iGlobalVerbosity = other.m_iGlobalVerbosity;
  m_iFlashVerbosity = other.m_iFlashVerbosity;
  m_iReadOnlyMode = other.m_iReadOnlyMode;
  m_iMaxErrorCount = other.m_iMaxErrorCount;
}

const LibpfOPCConfigurations & LibpfOPCConfigurations::operator==(const LibpfOPCConfigurations & other) {
  m_sHomePath = other.m_sHomePath;
  m_sKernelPath = other.m_sKernelPath;
  m_sCLSIDOfOPCServer = other.m_sCLSIDOfOPCServer;
  m_iSleepTime = other.m_iSleepTime;
  m_sTracePipe = other.m_sTracePipe;
  m_sTraceServer = other.m_sTraceServer;
  m_iGlobalVerbosity = other.m_iGlobalVerbosity;
  m_iFlashVerbosity = other.m_iFlashVerbosity;
  m_iReadOnlyMode = other.m_iReadOnlyMode;
  m_iMaxErrorCount = other.m_iMaxErrorCount;
  return *this;
}
