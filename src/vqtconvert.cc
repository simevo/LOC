/** @file VQTConvert.cc
    @brief Implementation of the VQTConvert class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based on:
      Converts between QT types and Windows Types
      Version: 1.0
      Modified on: 8-20-2009
      Created with: QT 4.5 and QT Creator 1.2
      Tested on: Windows XP SP3
      Bryan Cairns - August 2009

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */


#include <QString>

#ifdef Q_OS_WIN

#include "windows.h"

#include "vqtconvert.h"

namespace voidrealms {
namespace win32 {
//Convert a QString To LPCTSTR
LPCTSTR VQTConvert::QString_To_LPCTSTR(QString mQTData) {
  return (LPCTSTR)mQTData.utf16();
}

//Convert a QString To LPCSTR
LPCSTR VQTConvert::QString_To_LPCSTR(QString mQTData) {
  return (LPCSTR)mQTData.utf16();
}

//Convert a QString To LPTSTR
LPTSTR VQTConvert::QString_To_LPTSTR(QString mQTData) {
  return (LPTSTR)mQTData.utf16();
}

//Convert a LPCTSTR To QString
QString VQTConvert::LPCTSTR_To_QString(LPCTSTR mWinData) {
  return QString::fromUtf16((ushort*)mWinData);
}

//Convert a LPBYTE To QString
QString VQTConvert::LPBYTE_To_QString(LPBYTE mWinData) {
  return QString::fromUtf16((ushort*)mWinData);
}

//Convert a Char[] To QString
QString VQTConvert::Char_To_QString(char mWinData []) {
  return QString::fromUtf16((ushort*)mWinData);
}

//Convert a WCHAR* to a QString
QString VQTConvert::WCHAR_to_QString(WCHAR* mBuffer) {
  return QString::fromWCharArray(mBuffer);
}

//Convert a TCHAR To QString
QString VQTConvert::TChar_To_QString(TCHAR mWinData []) {
  return QString::fromUtf16((ushort*)mWinData);
}
}     //end voidrealms::win32 namespace
} //end voidrealms namespace

#endif // Q_OS_WIN
