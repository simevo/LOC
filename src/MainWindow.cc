/** @file MainWindow.cc
    @brief Implementation of the MainWindow class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QCloseEvent>

#include "MainWindow.h"
#include "MainWidget.h"
#include "AppConfig.h"
#include "MySettings.h"

static const int LOC_UI_VERSION = 1;

MainWindow::MainWindow(QString &Path, QWidget *parent, Qt::WindowFlags flags)
  : QMainWindow(parent, flags) {
  m_pMainWidget = new MainWidget(Path, this);

  setCentralWidget(m_pMainWidget);
  setWindowIcon(QIcon(":/LOC/images/icon.png"));
  setWindowTitle("LIBPF™ OPC Configurator");

  MySettings settings;
  if (settings.contains("geometry") && settings.contains("state")) {
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray(), LOC_UI_VERSION);
  }
}

MainWindow::~MainWindow() { }

void MainWindow::closeEvent(QCloseEvent* event) {
  if (this->m_pMainWidget->AskForQuit()) {
    MySettings settings;
    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState(LOC_UI_VERSION));
    event->accept();
  } else {
    event->ignore();
  }
}
