/** @file Appconfig.cc
    @brief Implementation of the AppConfig class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QApplication>

#include "AppConfig.h"

QStringList AppConfig::s_listUOMDefaultValues;

const QString AppConfig::s_sRegPathAppName = "LIBPF";
const QString AppConfig::s_sRegPathAppVersion = "1.0";
const QString AppConfig::s_sRegPathOrganization = "libpf.com";

const QString AppConfig::s_sRegKey_HomePath = "home_path";
const QString AppConfig::s_sRegKey_KernelPath = "kernel_path";
const QString AppConfig::s_sRegKey_CLSID = "CLSID";
const QString AppConfig::s_sRegKey_SleepTime = "sleepTime";
const QString AppConfig::s_sRegKey_TracePipe = "tracePipe";
const QString AppConfig::s_sRegKey_TraceServer = "traceServer";
const QString AppConfig::s_sRegKey_VerbosityGlobal = "verbosityGlobal";
const QString AppConfig::s_sRegKey_VerbosityFlash = "verbosityFlash";
const QString AppConfig::s_sRegKey_ReadOnly = "readOnly";
const QString AppConfig::s_sRegKey_MaxErrorCount = "maxErrCount";

const QString AppConfig::s_sImagesFolder = "images";
const QString AppConfig::s_sTempFileForValidating = "tffv.xml";
const QString AppConfig::s_sDefaultVariableGroupFileName = "settings.xml";
const QString AppConfig::s_sVariableGroupsFileXmlVersion = "1.0";
const QString AppConfig::s_sVariableGroupsFileXmlEncoding = "UTF-8";
const QString AppConfig::s_sXmlProcessingInstruction_StyleSheet = "type=\"text/xsl\" href=\"stylesheet.xsl\"";
const QString AppConfig::s_sXmlAttribute_GroupsLink = "xmlns:xsi";
const QString AppConfig::s_sXmlAttributeValue_GroupsLink = "http://www.w3.org/2001/XMLSchema-instance";
const QString AppConfig::s_sXmlAttribute_GroupsSchema = "xsi:noNamespaceSchemaLocation";
const QString AppConfig::s_sXmlAttributeValue_GroupsSchema = "schema.xsd";

const int AppConfig::s_iVerbosityMin = 0;
const int AppConfig::s_iVerbosityMax = 3;
const int AppConfig::s_iErrorCountMin = 0;
const int AppConfig::s_iErrorCountMax = 0x7fffffff;
const int AppConfig::s_iSleepTimeMin = 0;
const int AppConfig::s_iSleepTimeMax = 10000;
const QString AppConfig::s_sXmlTag_Groups = "groups";
const QString AppConfig::s_sXmlTag_Group = "group";
const QString AppConfig::s_sXmlTag_Input = "input";
const QString AppConfig::s_sXmlTag_Result = "result";
const QString AppConfig::s_sXmlTag_Constant = "constant";
const QString AppConfig::s_sXmlAttribute_GroupId = "id";
const QString AppConfig::s_sXmlAttribute_GroupEnabled = "enabled";
const QString AppConfig::s_sXmlAttribute_GroupDescription = "description";
const QString AppConfig::s_sXmlAttribute_GroupModel = "model";
const QString AppConfig::s_sXmlAttribute_VariableOPCtag = "OPCtag";
const QString AppConfig::s_sXmlAttribute_VariableDescription = "description";
const QString AppConfig::s_sXmlAttribute_VariableTAG = "TAG";
const QString AppConfig::s_sXmlAttribute_VariableUOM = "UOM";
const QString AppConfig::s_sXmlAttribute_VariableMin = "min";
const QString AppConfig::s_sXmlAttribute_VariableMax = "max";
const QString AppConfig::s_sServiceName = "LIBPFonOPC";
const int AppConfig::s_iServiceUpdateInterval = 500;

AppConfig::AppConfig(void)
{}

AppConfig::~AppConfig(void)
{}

const QStringList & AppConfig::GetDefaultUOMValuesList() {
  if (s_listUOMDefaultValues.length() == 0) {
    s_listUOMDefaultValues.append(QString(""));
    s_listUOMDefaultValues.append(QString("C"));
    s_listUOMDefaultValues.append(QString("K"));
    s_listUOMDefaultValues.append(QString("t/h"));
    s_listUOMDefaultValues.append(QString("kg/h"));
    s_listUOMDefaultValues.append(QString("kg/s"));
    s_listUOMDefaultValues.append(QString("barg"));
    s_listUOMDefaultValues.append(QString("bar"));
    s_listUOMDefaultValues.append(QString("atm"));
    s_listUOMDefaultValues.append(QString("m/s"));
    s_listUOMDefaultValues.append(QString("kg/m3"));
    s_listUOMDefaultValues.append(QString("m"));
    s_listUOMDefaultValues.append(QString("mm"));
  }
  return s_listUOMDefaultValues;
}

