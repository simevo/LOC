/** @file VariableGroup.cc
    @brief Implementation of the VariableGroup class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "VariableGroup.h"

VariableGroup::VariableGroup(void) {
  this->m_bIsEnabled = false;
  this->m_sDescription = "";
  this->m_sId = "";
  this->m_sModel = "";
}

VariableGroup::~VariableGroup()
{}

VariableGroup::VariableGroup(const VariableGroup &other) {
  this->m_bIsEnabled = other.m_bIsEnabled;
  this->m_sDescription = other.m_sDescription;
  this->m_sId = other.m_sId;
  this->m_sModel = other.m_sModel;
  this->m_listVariablesInput = other.m_listVariablesInput;
  this->m_listVariablesResult = other.m_listVariablesResult;
  this->m_listVariablesConstant = other.m_listVariablesConstant;
}

VariableGroup & VariableGroup::operator=(const VariableGroup &other) {
  this->m_bIsEnabled = other.m_bIsEnabled;
  this->m_sDescription = other.m_sDescription;
  this->m_sId = other.m_sId;
  this->m_sModel = other.m_sModel;
  this->m_listVariablesInput = other.m_listVariablesInput;
  this->m_listVariablesResult = other.m_listVariablesResult;
  this->m_listVariablesConstant = other.m_listVariablesConstant;
  return *this;
}
