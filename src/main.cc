/** @file main.cc
    @brief Main file

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QApplication>

#include "MainWindow.h"

int main(int argc, char *argv []) {
  QApplication a(argc, argv);
  QString appPath = a.applicationDirPath();
  MainWindow w(appPath);

  w.show();

  return a.exec();
}
