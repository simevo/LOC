/** @file TabControl.cc
    @brief Implementation of the TabControl class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QGridLayout>
#include <QApplication>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTimer>

#include "AppConfig.h"
#include "TabControl.h"

TabControl::TabControl(QWidget *parent)
  : QWidget(parent) {
  m_pLabelServiceStatus = new QLabel(tr("Status:"));
  m_pLineEditServiceStatus = new QLineEdit(this);
  m_pLineEditServiceStatus->setEnabled(false);
  // Get icons

  QPixmap pixStart(":/LOC/images/start_icon.png");
  QPixmap pixStop(":/LOC/images/stop_icon.png");
  m_iconStart.addPixmap(pixStart);
  m_iconStop.addPixmap(pixStop);

  // Create buttons
  QString sStart(tr("Start"));
  QString sStop(tr("Stop"));
  m_pButtonStart = new QPushButton(m_iconStart, sStart, this);
  m_pButtonStop = new QPushButton(m_iconStop, sStop, this);
  connect(m_pButtonStart, SIGNAL(clicked())
          , this, SLOT(StartService()));
  connect(m_pButtonStop, SIGNAL(clicked())
          , this, SLOT(StopService()));
  QGridLayout* pMainLayout = new QGridLayout();
  pMainLayout->addWidget(m_pLabelServiceStatus, 0,0);
  pMainLayout->addWidget(m_pLineEditServiceStatus, 0, 1);
  pMainLayout->addWidget(m_pButtonStart, 1, 0);
  pMainLayout->addWidget(m_pButtonStop, 2, 0);
  // pMainLayout->addWidget(new Spacer(), 3, 0);
  pMainLayout->setRowStretch(3, 1);
  setLayout(pMainLayout);
  
#ifdef Q_OS_WIN
  QStringList temp = m_ServiceManager.getServicesStringList();
  QString st = m_ServiceManager.getStatus(AppConfig::s_sServiceName);
#endif // Q_OS_WIN
  this->startTimer(AppConfig::s_iServiceUpdateInterval);
  timerEvent(NULL);
}

TabControl::~TabControl()
{}

void TabControl::UpdateServices() {
  ;
}

void TabControl::timerEvent(QTimerEvent * /* e */) {
#ifdef Q_OS_WIN
  QString status = this->m_ServiceManager.getStatus(AppConfig::s_sServiceName);
  if (status == SERVICE_STATUS_SERVICE_RUNNING) {
    this->m_pButtonStart->setEnabled(false);
    this->m_pButtonStop->setEnabled(true);
  } else if (status == SERVICE_STATUS_SERVICE_STOPPED) {
    this->m_pButtonStart->setEnabled(true);
    this->m_pButtonStop->setEnabled(false);
  } else {
    /*SERVICE_STATUS_CONTINUE_PENDING
       SERVICE_STATUS_PAUSE_PENDING
       SERVICE_STATUS_SERVICE_PAUSED
       SERVICE_STATUS_SERVICE_START_PENDING
       SERVICE_STATUS_SERVICE_STOP_PENDING
     */
    this->m_pButtonStart->setEnabled(false);
    this->m_pButtonStop->setEnabled(false);
  }
  QString sStatus = status;
  this->m_pLineEditServiceStatus->setText(GetServiceStatusAsString(sStatus));
#endif // Q_OS_WIN
}

void TabControl::StartService() {
#ifdef Q_OS_WIN
  m_ServiceManager.Start(AppConfig::s_sServiceName);
  this->m_pButtonStart->setEnabled(false);
#endif // Q_OS_WIN
}

void TabControl::StopService() {
#ifdef Q_OS_WIN
  m_ServiceManager.Stop(AppConfig::s_sServiceName);
  this->m_pButtonStop->setEnabled(false);
#endif // Q_OS_WIN
}

QString TabControl::GetServiceStatusAsString(const QString & sStatus) {
#ifdef Q_OS_WIN
  QString result = QString(tr(""));
  if (sStatus == SERVICE_STATUS_SERVICE_RUNNING) {
    result = QString(tr("Running"));
  } else if (sStatus == SERVICE_STATUS_SERVICE_PAUSED) {
    result = QString(tr("Paused"));
  } else if (sStatus == SERVICE_STATUS_SERVICE_STOPPED) {
    result = QString(tr("Stopped"));
  } else if (sStatus == SERVICE_STATUS_CONTINUE_PENDING) {
    result = QString(tr("Pending"));
  } else if (sStatus == SERVICE_STATUS_PAUSE_PENDING) {
    result = QString(tr("Waiting for service pause"));
  } else if (sStatus == SERVICE_STATUS_SERVICE_START_PENDING) {
    result = QString(tr("Waiting for service start"));
  } else if (sStatus == SERVICE_STATUS_SERVICE_STOP_PENDING) {
    result = QString(tr("Waiting for service stop"));
  } else {
    result = QString(tr("Unknown"));
  }  
  return result;
#else
  return QString("");
#endif // Q_OS_WIN
}
