/** @file MainWidget.cc
    @brief Implementation of the MainWidget class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QFile>
#include <QVBoxLayout>
#include <QtGlobal>
#include <QFile>
#include <QStringRef>
#include <QTabWidget>

#include "TabConfigure.h"
#include "TabControl.h"
#include "TabVariables.h"
#include "VariableGroupXmlFileController.h"
#include "AppConfig.h"
#include "MainWidget.h"
#include "MySettings.h"

MainWidget::MainWidget(QString &sAppPath, QWidget *parent)
  : QWidget(parent) {
  m_sAppPath = sAppPath;
  this->m_pMainTab = new QTabWidget();

  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(m_pMainTab);
  setLayout(layout);

  this->m_pTabVariables = new TabVariables(this);
  MySettings settings;
  QString sVariableGroupFilePath = settings.value(AppConfig::s_sRegKey_HomePath).toString();
  sVariableGroupFilePath += "/";
  sVariableGroupFilePath += AppConfig::s_sDefaultVariableGroupFileName;
  m_pTabVariables->SetDefaultVariableGroupFile(sVariableGroupFilePath);
  m_pTabVariables->LoadDefaultVariableGroupFile();

  // pTabVariables->SetData(&this->m_listVariableGroups);
  this->m_pMainTab->addTab(new TabControl(this), tr("Start/Stop"));
  this->m_pMainTab->addTab(new TabConfigure(this), tr("Settings"));
  this->m_pMainTab->addTab(m_pTabVariables, tr("Variables"));
}

MainWidget::~MainWidget() {
  VariableGroupXmlFileController::ReleaseSingleton();
}

bool MainWidget::AskForQuit() {
  return this->m_pTabVariables->AskForLeaveUnsavedWork(tr("Are you sure you want to quit ?"));
}
