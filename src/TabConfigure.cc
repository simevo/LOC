/** @file TabConfigure.cc
    @brief Implementation of the TabConfigure class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QLabel>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QApplication>
#include <QDir>
#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QSlider>
#include <QFileDialog>
#include <QStatusBar>

#include "AppConfig.h"
#include "FolderPathValidator.h"
#include "TabConfigure.h"
#include "MySettings.h"

TabConfigure::TabConfigure(QWidget *parent)
  : QWidget(parent)
  ,m_sNoticeKey(tr("Key "))
  ,m_sNoticeSetTo(tr(" has been set to "))
  ,m_sNoticeRestart(tr(", restart service to make change effective!")) {
  InitControls();
  this->m_Configs = new LibpfOPCConfigurations();
  // Setup background colors
  m_ColorInactiveValid = this->m_pLineEditHomePath->palette().color(QPalette::Base);
  m_ColorActiveValid = this->m_pLineEditSleepTime->palette().color(QPalette::Base);
  m_ColorInvalid = QColor(Qt::red);
  m_bIsLoading = false;
  QMainWindow* mainWindow = (QMainWindow*)this->parent()->parent();
  m_pStatusBar = mainWindow->statusBar();
  LoadConfig();
}

void TabConfigure::LoadConfig() {
  m_bIsLoading = true;
  MySettings settings;
  QStringList keys = settings.allKeys();
  if (keys.size() > 0) {
    this->m_Configs->m_sHomePath = settings.value(AppConfig::s_sRegKey_HomePath,"").value<QString>();
    this->m_Configs->m_sKernelPath = settings.value(AppConfig::s_sRegKey_KernelPath, "").value<QString>();
    this->m_Configs->m_sCLSIDOfOPCServer = settings.value(AppConfig::s_sRegKey_CLSID, "").value<QString>();
    this->m_Configs->m_iSleepTime = settings.value(AppConfig::s_sRegKey_SleepTime, "").value<int>();
    this->m_Configs->m_sTracePipe = settings.value(AppConfig::s_sRegKey_TracePipe, "").value<QString>();
    this->m_Configs->m_sTraceServer = settings.value(AppConfig::s_sRegKey_TraceServer, "").value<QString>();
    this->m_Configs->m_iGlobalVerbosity = settings.value(AppConfig::s_sRegKey_VerbosityGlobal,0).value<int>();
    this->m_Configs->m_iFlashVerbosity = settings.value(AppConfig::s_sRegKey_VerbosityFlash, 0).value<int>();
    this->m_Configs->m_iReadOnlyMode = settings.value(AppConfig::s_sRegKey_ReadOnly, 0).value<int>();
    this->m_Configs->m_iMaxErrorCount = settings.value(AppConfig::s_sRegKey_MaxErrorCount, 0).value<int>();
  } else {
    settings.setValue(AppConfig::s_sRegKey_HomePath, "");
    settings.setValue(AppConfig::s_sRegKey_KernelPath, "");
    settings.setValue(AppConfig::s_sRegKey_CLSID, "");
    settings.setValue(AppConfig::s_sRegKey_SleepTime, 0);
    settings.setValue(AppConfig::s_sRegKey_TracePipe, "");
    settings.setValue(AppConfig::s_sRegKey_TraceServer, "");
    settings.setValue(AppConfig::s_sRegKey_VerbosityGlobal, 0);
    settings.setValue(AppConfig::s_sRegKey_VerbosityFlash, 0);
    settings.setValue(AppConfig::s_sRegKey_ReadOnly, 0);
    settings.setValue(AppConfig::s_sRegKey_MaxErrorCount, 0);
    settings.sync();
  }
  // Set configurations to controls
  this->m_pLineEditHomePath->setText(this->m_Configs->m_sHomePath);
  this->m_pLineEditKernelPath->setText(this->m_Configs->m_sKernelPath);
  this->m_pLineEditCLSIDOfOPCServer->setText(this->m_Configs->m_sCLSIDOfOPCServer);
  // Sleep time
  if (this->m_Configs->m_iSleepTime > AppConfig::s_iSleepTimeMax) {
    SetStateInvalid(this->m_pLineEditSleepTime);
  } else if (this->m_Configs->m_iSleepTime < AppConfig::s_iSleepTimeMin) {
    SetStateInvalid(this->m_pLineEditSleepTime);
  } else {
    SetStateValid(this->m_pLineEditSleepTime);
  }
  this->m_pLineEditSleepTime->setText(QString::number(this->m_Configs->m_iSleepTime));

  this->m_pLineEditTracePipe->setText(this->m_Configs->m_sTracePipe);
  this->m_pLineEditTraceServer->setText(this->m_Configs->m_sTraceServer);
  // Flash verbosity
  if (this->m_Configs->m_iFlashVerbosity > AppConfig::s_iVerbosityMax) {
    SetStateInvalid(m_pSpinBoxFlashVerbosity);
    this->m_pSpinBoxFlashVerbosity->setMaximum(this->m_Configs->m_iFlashVerbosity);
  } else if (this->m_Configs->m_iFlashVerbosity < AppConfig::s_iVerbosityMin) {
    SetStateInvalid(m_pSpinBoxFlashVerbosity);
    this->m_pSpinBoxFlashVerbosity->setMinimum(this->m_Configs->m_iFlashVerbosity);
  } else {
    SetStateValid(m_pSpinBoxFlashVerbosity);
  }
  this->m_pSpinBoxFlashVerbosity->setValue(this->m_Configs->m_iFlashVerbosity);
  // Global verbosity
  if (this->m_Configs->m_iGlobalVerbosity > AppConfig::s_iVerbosityMax) {
    SetStateInvalid(m_pSpinBoxGlobalVerbosity);
    this->m_pSpinBoxGlobalVerbosity->setMaximum(this->m_Configs->m_iGlobalVerbosity);
  } else if (this->m_Configs->m_iGlobalVerbosity < AppConfig::s_iVerbosityMin) {
    SetStateInvalid(m_pSpinBoxGlobalVerbosity);
    this->m_pSpinBoxGlobalVerbosity->setMinimum(this->m_Configs->m_iGlobalVerbosity);
  } else {
    SetStateValid(m_pSpinBoxGlobalVerbosity);
  }
  this->m_pSpinBoxGlobalVerbosity->setValue(this->m_Configs->m_iGlobalVerbosity);

  if (this->m_Configs->m_iReadOnlyMode == 0) {
    this->m_pCheckBoxReadOnlyMode->setCheckState(Qt::Unchecked);
  } else {
    this->m_pCheckBoxReadOnlyMode->setCheckState(Qt::Checked);
  }
  // Max error count
  if (this->m_Configs->m_iMaxErrorCount > AppConfig::s_iErrorCountMax) {
    SetStateInvalid(m_pSpinBoxMaxErrorCount);
    this->m_pSpinBoxMaxErrorCount->setMaximum(this->m_Configs->m_iMaxErrorCount);
  } else if (this->m_Configs->m_iMaxErrorCount < AppConfig::s_iErrorCountMin) {
    SetStateInvalid(m_pSpinBoxMaxErrorCount);
    this->m_pSpinBoxMaxErrorCount->setMinimum(this->m_Configs->m_iMaxErrorCount);
  } else {
    SetStateValid(m_pSpinBoxMaxErrorCount);
  }
  this->m_pSpinBoxMaxErrorCount->setValue(this->m_Configs->m_iMaxErrorCount);

  m_bIsLoading = false;
}

void TabConfigure::SaveConfig() {
  MySettings settings;
  settings.setValue(AppConfig::s_sRegKey_HomePath, this->m_Configs->m_sHomePath);
  settings.setValue(AppConfig::s_sRegKey_KernelPath, this->m_Configs->m_sKernelPath);
  settings.setValue(AppConfig::s_sRegKey_CLSID, this->m_Configs->m_sCLSIDOfOPCServer);
  settings.setValue(AppConfig::s_sRegKey_SleepTime, this->m_Configs->m_iSleepTime);
  settings.setValue(AppConfig::s_sRegKey_TracePipe, this->m_Configs->m_sTracePipe);
  settings.setValue(AppConfig::s_sRegKey_TraceServer, this->m_Configs->m_sTraceServer);
  settings.setValue(AppConfig::s_sRegKey_VerbosityGlobal, this->m_Configs->m_iGlobalVerbosity);
  settings.setValue(AppConfig::s_sRegKey_VerbosityFlash, this->m_Configs->m_iFlashVerbosity);
  settings.setValue(AppConfig::s_sRegKey_ReadOnly, this->m_Configs->m_iReadOnlyMode);
  settings.setValue(AppConfig::s_sRegKey_MaxErrorCount, this->m_Configs->m_iMaxErrorCount);
  settings.sync();
}

void TabConfigure::InitControls() {
  QGridLayout* pMainLayout = new QGridLayout();
  QLabel* pLabelHomePath = new QLabel(tr("Home path"));
  QLabel* pLabelKernelPath = new QLabel(tr("Kernel path"));
  QLabel* pLabelCLSIDOfOPC = new QLabel(tr("CLSID of the OPC server"));
  QLabel* pLabelSleepTime = new QLabel(tr("Sleep time"));
  QLabel* pLabelTraceServer = new QLabel(tr("Trace server"));
  QLabel* pLabelTracePipe = new QLabel(tr("Trace pipe"));
  QLabel* pLabelGlobalVerbosity = new QLabel(tr("Global verbosity"));
  QLabel* pLabelFlashVerbosity = new QLabel(tr("Flash verbosity"));
  QLabel* pReadOnlyMode = new QLabel(tr("Read only mode"));
  QLabel* pLabelMaximumErrorCount = new QLabel(tr("Maximum error count"));

  m_pLineEditHomePath = new QLineEdit(this);
  m_pLineEditHomePath->setEnabled(false);
  m_pLineEditHomePath->setAlignment(Qt::AlignLeft);
  m_pLineEditKernelPath = new QLineEdit(this);
  QPixmap pix(":/LOC/images/folder_icon.png");
  m_pButtonHomePath = new QPushButton(QIcon(pix), tr(""), this);
  m_pButtonKernelPath = new QPushButton(QIcon(pix), tr(""), this);

  m_pLineEditKernelPath->setEnabled(false);
  m_pLineEditKernelPath->setAlignment(Qt::AlignLeft);

  QHBoxLayout *pLayoutHomePath = new QHBoxLayout();
  pLayoutHomePath->addWidget(m_pLineEditHomePath);
  pLayoutHomePath->addWidget(m_pButtonHomePath);
  pLayoutHomePath->setAlignment(Qt::AlignRight);

  QHBoxLayout *pLayoutKernelPath = new QHBoxLayout();
  pLayoutKernelPath->addWidget(m_pLineEditKernelPath);
  pLayoutKernelPath->addWidget(m_pButtonKernelPath);
  pLayoutKernelPath->setAlignment(Qt::AlignRight);

  m_pLineEditCLSIDOfOPCServer = new QLineEdit(this);
  m_pLineEditSleepTime = new QLineEdit(this);
  m_pLineEditSleepTime->setFixedWidth(50);
  m_pSleepTimeValidator = new QIntValidator(AppConfig::s_iSleepTimeMin, AppConfig::s_iSleepTimeMax, this);
  m_pLineEditSleepTime->setValidator(m_pSleepTimeValidator);
  m_pLineEditTraceServer = new QLineEdit(this);
  m_pLineEditTracePipe = new QLineEdit(this);

  m_pSpinBoxGlobalVerbosity = new QSpinBox(this);
  m_pSpinBoxGlobalVerbosity->setFixedWidth(75);
  m_pSpinBoxGlobalVerbosity->setMinimum(AppConfig::s_iVerbosityMin);
  m_pSpinBoxGlobalVerbosity->setMaximum(AppConfig::s_iVerbosityMax);

  m_pSpinBoxFlashVerbosity = new QSpinBox(this);
  m_pSpinBoxFlashVerbosity->setFixedWidth(75);
  m_pSpinBoxFlashVerbosity->setMinimum(AppConfig::s_iVerbosityMin);
  m_pSpinBoxFlashVerbosity->setMaximum(AppConfig::s_iVerbosityMax);

  m_pSpinBoxMaxErrorCount = new QSpinBox(this);
  m_pSpinBoxMaxErrorCount->setFixedWidth(75);
  m_pSpinBoxMaxErrorCount->setMinimum(AppConfig::s_iErrorCountMin);
  m_pSpinBoxMaxErrorCount->setMaximum(AppConfig::s_iErrorCountMax);

  m_pCheckBoxReadOnlyMode = new QCheckBox(this);

  m_pSliderSleepTime = new QSlider(Qt::Horizontal, this);
  m_pSliderSleepTime->setFixedWidth(200);
  m_pSliderSleepTime->setMinimum(AppConfig::s_iSleepTimeMin);
  m_pSliderSleepTime->setMaximum(AppConfig::s_iSleepTimeMax);
  m_pSliderSleepTime->setTickInterval(1);

  pMainLayout->addWidget(pLabelHomePath, 0, 0);
  pMainLayout->addLayout(pLayoutHomePath, 0, 1);

  pMainLayout->addWidget(pLabelKernelPath, 1, 0);
  pMainLayout->addLayout(pLayoutKernelPath, 1, 1);

  pMainLayout->addWidget(pLabelCLSIDOfOPC, 2, 0);
  pMainLayout->addWidget(m_pLineEditCLSIDOfOPCServer, 2, 1);

  pMainLayout->addWidget(pLabelSleepTime, 3, 0);
  QHBoxLayout* pLayoutSleepTime = new QHBoxLayout();
  pLayoutSleepTime->setSpacing(2);

  pLayoutSleepTime->addWidget(m_pSliderSleepTime, Qt::AlignRight);
  pLayoutSleepTime->addWidget(m_pLineEditSleepTime, Qt::AlignRight);
  pMainLayout->addLayout(pLayoutSleepTime, 3, 1, Qt::AlignRight);

  pMainLayout->addWidget(pLabelTraceServer, 4, 0);
  pMainLayout->addWidget(m_pLineEditTraceServer, 4, 1);

  pMainLayout->addWidget(pLabelTracePipe, 5, 0);
  pMainLayout->addWidget(m_pLineEditTracePipe, 5, 1);

  pMainLayout->addWidget(pLabelGlobalVerbosity, 6, 0);
  pMainLayout->addWidget(m_pSpinBoxGlobalVerbosity, 6, 1, Qt::AlignRight);

  pMainLayout->addWidget(pLabelFlashVerbosity, 7, 0);
  pMainLayout->addWidget(m_pSpinBoxFlashVerbosity, 7, 1, Qt::AlignRight);

  pMainLayout->addWidget(pReadOnlyMode, 8, 0);
  pMainLayout->addWidget(m_pCheckBoxReadOnlyMode, 8, 1, Qt::AlignRight);

  pMainLayout->addWidget(pLabelMaximumErrorCount, 9, 0);
  pMainLayout->addWidget(m_pSpinBoxMaxErrorCount, 9, 1, Qt::AlignRight);

  pMainLayout->setRowStretch(10, 1);

  // File dialog
  m_pFileDialog = new QFileDialog(this);
  m_pFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
  m_pFileDialog->setFileMode(QFileDialog::DirectoryOnly);

  this->setLayout(pMainLayout);
  // Setup events
  connect(m_pButtonHomePath, SIGNAL(clicked(bool)),
          this, SLOT(HandleButtonClickedHomePath(bool)));
  connect(m_pButtonKernelPath, SIGNAL(clicked(bool)),
          this, SLOT(HandleButtonClickedKernelPath(bool)));

  connect(this->m_pLineEditHomePath, SIGNAL(textChanged(const QString &))
          , this, SLOT(HandleFolderTextChanged(const QString &)));

  connect(this->m_pLineEditKernelPath, SIGNAL(textChanged(const QString &))
          , this, SLOT(HandleFolderTextChanged(const QString &)));
  connect(this->m_pSliderSleepTime, SIGNAL(sliderReleased())
          , this, SLOT(HandleSleepTimeSliderReleased()));
  connect(this->m_pLineEditSleepTime, SIGNAL(textChanged(const QString &))
          , this, SLOT(HandleSleepTimeTextChanged(const QString &)));
  connect(this->m_pLineEditSleepTime, SIGNAL(editingFinished())
          , this, SLOT(HandleTraceSleedTimeEditingFinished()));

  connect(this->m_pSpinBoxFlashVerbosity, SIGNAL(valueChanged(int))
          , this, SLOT(HandleFlashVebosityValueChanged(int)));
  connect(this->m_pSpinBoxGlobalVerbosity, SIGNAL(valueChanged(int))
          , this, SLOT(HandleGlobalVebosityValueChanged(int)));
  connect(this->m_pSpinBoxMaxErrorCount, SIGNAL(valueChanged(int))
          , this, SLOT(HandleMaxErrorCountValueChanged(int)));

  connect(this->m_pCheckBoxReadOnlyMode, SIGNAL(stateChanged(int))
          , this, SLOT(HandleReadOnlyModeValueChanged(int)));

  connect(this->m_pLineEditCLSIDOfOPCServer, SIGNAL(textChanged(const QString &))
          , this, SLOT(HandleCLSIDServerValueChanged(const QString &)));

  connect(this->m_pLineEditTracePipe, SIGNAL(textChanged(const QString &))
          , this, SLOT(HandleTracePipeValueChanged(const QString &)));
  connect(this->m_pLineEditTraceServer, SIGNAL(textChanged(const QString &))
          , this, SLOT(HandleTraceServerValueChanged(const QString &)));
}

TabConfigure::~TabConfigure()
{}

void TabConfigure::HandleButtonClickedHomePath(bool /* bIsChecked */) {
  QDir homeDir;
  if (homeDir.exists(this->m_pLineEditHomePath->text())) {
    m_pFileDialog->setDirectory(this->m_pLineEditHomePath->text());
  } else {
    m_pFileDialog->setDirectory(QApplication::applicationDirPath());
  }
  if (m_pFileDialog->exec()) {
    QStringList fileNames;
    fileNames = m_pFileDialog->selectedFiles();

    if (fileNames.size() > 0) {
      this->m_pLineEditHomePath->setText(fileNames.at(0));
    }
  }
}

void TabConfigure::HandleFolderTextChanged(const QString & text) {
  QObject* sd = QObject::sender();
  if (sd == m_pLineEditHomePath) {
    if (!this->IsFolderPathValid(m_pLineEditHomePath->text())) {
      QPalette pal = m_pLineEditKernelPath->palette();
      pal.setColor(QPalette::Base, this->m_ColorInvalid);
      m_pLineEditHomePath->setPalette(pal);
    } else {
      QPalette pal = m_pLineEditKernelPath->palette();
      pal.setColor(QPalette::Base, this->m_ColorInactiveValid);
      m_pLineEditHomePath->setPalette(pal);
    }
    if (this->m_Configs->m_sHomePath != text) {
      this->m_Configs->m_sHomePath = text;
      QString sStatusMessage = this->m_sNoticeKey
                               + AppConfig::s_sRegKey_HomePath
                               + this->m_sNoticeSetTo
                               + this->m_Configs->m_sHomePath
                               + this->m_sNoticeRestart;
      this->m_pStatusBar->showMessage(sStatusMessage);
      SaveConfig();
    }
  } else if (sd == m_pLineEditKernelPath) {
    if (!this->IsFolderPathValid(m_pLineEditKernelPath->text())) {
      QPalette pal = m_pLineEditKernelPath->palette();
      pal.setColor(QPalette::Base, this->m_ColorInvalid);
      m_pLineEditKernelPath->setPalette(pal);
    } else {
      QPalette pal = m_pLineEditKernelPath->palette();
      pal.setColor(QPalette::Base, this->m_ColorInactiveValid);
      m_pLineEditKernelPath->setPalette(pal);
    }
    if (this->m_Configs->m_sKernelPath != text) {
      this->m_Configs->m_sKernelPath = text;
      QString sStatusMessage = this->m_sNoticeKey
                               + AppConfig::s_sRegKey_KernelPath
                               + this->m_sNoticeSetTo
                               + this->m_Configs->m_sKernelPath
                               + this->m_sNoticeRestart;
      this->m_pStatusBar->showMessage(sStatusMessage);
      SaveConfig();
    }
  }
}

void TabConfigure::HandleButtonClickedKernelPath(bool /* bIsChecked */) {
  QDir kernelDir;
  if (kernelDir.exists(this->m_pLineEditKernelPath->text())) {
    m_pFileDialog->setDirectory(this->m_pLineEditKernelPath->text());
  } else {
    m_pFileDialog->setDirectory(QApplication::applicationDirPath());
  }
  if (m_pFileDialog->exec()) {
    QStringList fileNames;
    fileNames = m_pFileDialog->selectedFiles();
    if (fileNames.size() > 0) {
      this->m_pLineEditKernelPath->setText(fileNames.at(0));
    }
  }
}

bool TabConfigure::IsFolderPathValid(const QString & sPath) {
  QDir dirPath = QDir(sPath);
  return dirPath.exists();
}

void TabConfigure::HandlerSleepTimeSliderChanged(int Value) {
  QString text = QString::number(Value);
  this->m_pLineEditSleepTime->setText(text);
  int iNewValue = this->m_pLineEditSleepTime->text().toInt();
  if (!m_bIsLoading
      && this->m_Configs->m_iSleepTime != iNewValue) {
    this->m_Configs->m_iSleepTime = iNewValue; // ???
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_SleepTime
                             + this->m_sNoticeSetTo
                             + this->m_pLineEditSleepTime->text()
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleSleepTimeSliderReleased() {
  int Value = this->m_pSliderSleepTime->value();
  QString text = QString::number(Value);
  this->m_pLineEditSleepTime->setText(text);
  int iNewValue = this->m_pLineEditSleepTime->text().toInt();
  if (!m_bIsLoading
      && this->m_Configs->m_iSleepTime != iNewValue) {
    this->m_Configs->m_iSleepTime = iNewValue; // ???
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_SleepTime
                             + this->m_sNoticeSetTo
                             + this->m_pLineEditSleepTime->text()
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleSleepTimeTextChanged(const QString & sValue) {
  int pos = 0;
  // QObject* sd = QObject::sender();
  QString sValueMutable(sValue);
  QValidator::State st = this->m_pSleepTimeValidator->validate(sValueMutable, pos);
  if (st == QValidator::Acceptable) {
    QPalette pal = m_pLineEditKernelPath->palette();
    pal.setColor(QPalette::Base, this->m_ColorActiveValid);
    m_pLineEditSleepTime->setPalette(pal);
    int sliderValue = sValue.toInt();
    if (sliderValue != this->m_pSliderSleepTime->value()) {
      this->m_pSliderSleepTime->setValue(sliderValue);
    }
    int iNewValue = this->m_pLineEditSleepTime->text().toInt();
    if (!m_bIsLoading
        && this->m_Configs->m_iSleepTime != iNewValue) {
      this->m_Configs->m_iSleepTime = iNewValue;
      QString sStatusMessage = this->m_sNoticeKey
                               + AppConfig::s_sRegKey_SleepTime
                               + this->m_sNoticeSetTo
                               + this->m_pLineEditSleepTime->text()
                               + this->m_sNoticeRestart;
      this->m_pStatusBar->showMessage(sStatusMessage);
      SaveConfig();
    }
  } else {
    QPalette pal = m_pLineEditKernelPath->palette();
    pal.setColor(QPalette::Base, this->m_ColorInvalid);
    m_pLineEditSleepTime->setPalette(pal);
  }
}

void TabConfigure::HandleFlashVebosityValueChanged(int iValue) {
  if (m_bIsLoading) {
    return;
  }
  m_pSpinBoxFlashVerbosity->setMinimum(AppConfig::s_iVerbosityMin);
  m_pSpinBoxFlashVerbosity->setMaximum(AppConfig::s_iVerbosityMax);
  this->SetStateValid(m_pSpinBoxFlashVerbosity);
  iValue = this->m_pSpinBoxFlashVerbosity->value();
  if (this->m_Configs->m_iFlashVerbosity != iValue) {
    this->m_Configs->m_iFlashVerbosity = iValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_VerbosityFlash
                             + this->m_sNoticeSetTo
                             + QString::number(this->m_Configs->m_iFlashVerbosity)
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleGlobalVebosityValueChanged(int iValue) {
  if (m_bIsLoading) {
    return;
  }
  m_pSpinBoxGlobalVerbosity->setMinimum(AppConfig::s_iVerbosityMin);
  m_pSpinBoxGlobalVerbosity->setMaximum(AppConfig::s_iVerbosityMax);
  this->SetStateValid(m_pSpinBoxGlobalVerbosity);
  iValue = this->m_pSpinBoxGlobalVerbosity->value();
  if (this->m_Configs->m_iGlobalVerbosity != iValue) {
    this->m_Configs->m_iGlobalVerbosity = iValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_VerbosityGlobal
                             + this->m_sNoticeSetTo
                             + QString::number(this->m_Configs->m_iGlobalVerbosity)
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleMaxErrorCountValueChanged(int iValue) {
  if (m_bIsLoading) {
    return;
  }
  m_pSpinBoxMaxErrorCount->setMinimum(AppConfig::s_iErrorCountMin);
  m_pSpinBoxMaxErrorCount->setMaximum(AppConfig::s_iErrorCountMax);
  this->SetStateValid(m_pSpinBoxMaxErrorCount);
  if (this->m_Configs->m_iMaxErrorCount != iValue) {
    this->m_Configs->m_iMaxErrorCount = iValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_MaxErrorCount
                             + this->m_sNoticeSetTo
                             + QString::number(this->m_Configs->m_iMaxErrorCount)
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleReadOnlyModeValueChanged(int iValue) {
  int iNewValue;
  if (iValue == 2) {
    iNewValue = 1;
  } else {
    iNewValue = 0;
  }
  if (!m_bIsLoading
      && this->m_Configs->m_iReadOnlyMode != iNewValue) {
    this->m_Configs->m_iReadOnlyMode = iNewValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_ReadOnly
                             + this->m_sNoticeSetTo
                             + QString::number(this->m_Configs->m_iReadOnlyMode)
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleCLSIDServerValueChanged(const QString & sValue) {
  if (!m_bIsLoading
      && this->m_Configs->m_sCLSIDOfOPCServer != sValue) {
    this->m_Configs->m_sCLSIDOfOPCServer = sValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_CLSID
                             + this->m_sNoticeSetTo
                             + sValue
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleTracePipeValueChanged(const QString & sValue) {
  if (!m_bIsLoading
      && this->m_Configs->m_sTracePipe != sValue) {
    this->m_Configs->m_sTracePipe = sValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_TracePipe
                             + this->m_sNoticeSetTo
                             + sValue
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleTraceServerValueChanged(const QString & sValue) {
  if (!m_bIsLoading
      && this->m_Configs->m_sTraceServer != sValue) {
    this->m_Configs->m_sTraceServer = sValue;
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_TraceServer
                             + this->m_sNoticeSetTo
                             + sValue
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::HandleTraceSleedTimeEditingFinished() {
  this->m_Configs->m_iSleepTime = this->m_pLineEditSleepTime->text().toInt();
  if (this->m_Configs->m_iSleepTime > AppConfig::s_iSleepTimeMax) {
    SetStateInvalid(this->m_pLineEditSleepTime);
  } else if (this->m_Configs->m_iSleepTime < AppConfig::s_iSleepTimeMin) {
    SetStateInvalid(this->m_pLineEditSleepTime);
  } else {
    SetStateValid(this->m_pLineEditSleepTime);
  }
  int iNewValue = this->m_pLineEditSleepTime->text().toInt();
  if (!m_bIsLoading
      && this->m_Configs->m_iSleepTime != iNewValue) {
    this->m_Configs->m_iSleepTime = iNewValue; // ???
    QString sStatusMessage = this->m_sNoticeKey
                             + AppConfig::s_sRegKey_SleepTime
                             + this->m_sNoticeSetTo
                             + this->m_pLineEditSleepTime->text()
                             + this->m_sNoticeRestart;
    this->m_pStatusBar->showMessage(sStatusMessage);
    SaveConfig();
  }
}

void TabConfigure::SetStateValid(QWidget* pTarget) {
  QPalette pal = pTarget->palette();
  pal.setColor(QPalette::Base, this->m_ColorActiveValid);
  pTarget->setPalette(pal);
}

void TabConfigure::SetStateInvalid(QWidget* pTarget) {
  QPalette pal = pTarget->palette();
  pal.setColor(QPalette::Base, this->m_ColorInvalid);
  pTarget->setPalette(pal);
}

void TabConfigure::SetStateInactiveValid(QWidget* pTarget) {
  QPalette pal = pTarget->palette();
  pal.setColor(QPalette::Base, this->m_ColorInactiveValid);
  pTarget->setPalette(pal);
}
