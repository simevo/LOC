/** @file MySettings.cc
    @brief Implementation of the MySettings class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "MySettings.h"
#include "AppConfig.h"

MySettings::MySettings(void) : QSettings(QSettings::SystemScope, AppConfig::s_sRegPathOrganization, AppConfig::s_sRegPathAppName) {
  beginGroup(AppConfig::s_sRegPathAppVersion);
}

QString MySettings::schemaFileName(void) const {
  QString sSchemaFileName = value(AppConfig::s_sRegKey_HomePath).toString();
  sSchemaFileName += "/";
  sSchemaFileName += AppConfig::s_sXmlAttributeValue_GroupsSchema;
  return sSchemaFileName;
}
