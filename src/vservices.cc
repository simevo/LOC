/** @file VServices.cc
    @brief Implementation of the wrapper for the Windows Services

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based on:
      Wrapper for the Windows Services
      Version: 1.0
      Modified on: 8-20-2009
      Created with: QT 4.5 and QT Creator 1.2
      Tested on: Windows XP SP3
      Bryan Cairns

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QString>

#ifdef Q_OS_WIN

#include <QStringList>
#include <QDebug>

#include "windows.h"

#include "vservices.h"
#include "vqtconvert.h"

namespace voidrealms {
namespace win32 {
//Default Constructor
VServices::VServices() {
  this->mComputername = ".";
}

//Constructor for remote computers
VServices::VServices(QString Computername) {
  this->mComputername = Computername;
}

//Opens the service manager on the specified computer
bool VServices::OpenManager() {
  LPCTSTR mMachine = NULL;

  //If the Computername is not the local computer, set the mMachinename
  if (this->mComputername != "" && this->mComputername != ".") {
    mMachine = VQTConvert::QString_To_LPCTSTR(this->mComputername);
  }
  this->mManager = OpenSCManager(mMachine,SERVICES_ACTIVE_DATABASE,SC_MANAGER_ALL_ACCESS);

  if (this->mManager == NULL) {
    return false;
  } else {
    return true;
  }
}

//Closes the service manager
bool VServices::CloseManager() {
  return CloseServiceHandle(this->mManager);
}

//Returns a QStringList filled with installed service names
QStringList VServices::getServicesStringList() {
  QStringList mList;

  //Open the service manager
  if (OpenManager()) {
    LPENUM_SERVICE_STATUS lpServices = NULL;
    BOOL bOK;
    DWORD dwBufferSize = 0;
    DWORD dwNumberServices = 0;
    DWORD dwResume = 0;

    //Call the function to get the buffer size
    bOK = EnumServicesStatus(mManager,SERVICE_WIN32,SERVICE_STATE_ALL,lpServices, 0,&dwBufferSize, &dwNumberServices,&dwResume);

    //Create the buffer and fill it with the service list
    lpServices = (LPENUM_SERVICE_STATUS) new BYTE[dwBufferSize];
    bOK = EnumServicesStatus(mManager, SERVICE_WIN32, SERVICE_STATE_ALL, lpServices, dwBufferSize, &dwBufferSize,&dwNumberServices, &dwResume);

    for (int i = 0; i < (int)dwNumberServices; i++) {
      QString mService = VQTConvert::LPCTSTR_To_QString(lpServices[i].lpServiceName);
      mList.append(mService);
    }
    //Cleanup
    delete [] lpServices;
    this->CloseManager();
  }
  return mList;
}

//Gets the services display name
QString VServices::getDisplayName(QString ServiceName) {
  QString mReturn = "";

  //open the service manager
  if (OpenManager()) {
    LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);
    DWORD dwSize = 255;
    WCHAR* pBuffer = new WCHAR[dwSize];

    //Get the buffer size
    GetServiceDisplayName(mManager,mServiceName,pBuffer,&dwSize);

    //Convert the buffer to a QString
    mReturn = VQTConvert::WCHAR_to_QString(pBuffer);

    //Cleanup
    delete [] pBuffer;

    //close the service manager
    this->CloseManager();
  }
  return mReturn;
}

//Returns a QString with the services status
QString VServices::getStatus(QString ServiceName) {
  QString mReturn = "";

  //Open the service manager
  if (OpenManager()) {
    LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

    //Open the Service
    SC_HANDLE mOpenService = OpenService(mManager,mServiceName,SERVICE_QUERY_STATUS);
    if (mOpenService != NULL) {
      SERVICE_STATUS serviceStatus;
      if (QueryServiceStatus(mOpenService,&serviceStatus)) {
        switch (serviceStatus.dwCurrentState) {
        case SERVICE_CONTINUE_PENDING:
          mReturn = "SERVICE_CONTINUE_PENDING";
          break;
        case SERVICE_PAUSE_PENDING:
          mReturn = "SERVICE_PAUSE_PENDING";
          break;
        case SERVICE_PAUSED:
          mReturn = "SERVICE_PAUSED";
          break;
        case SERVICE_RUNNING:
          mReturn = "SERVICE_RUNNING";
          break;
        case SERVICE_START_PENDING:
          mReturn = "SERVICE_START_PENDING";
          break;
        case SERVICE_STOP_PENDING:
          mReturn = "SERVICE_STOP_PENDING";
          break;
        case SERVICE_STOPPED:
          mReturn = "SERVICE_STOPPED";
          break;
        }
      }
      //Close the Service
      CloseServiceHandle(mOpenService);
    }
    //Close the service manager
    this->CloseManager();
  }
  return mReturn;
}

//Returns a QString with the services start type
QString VServices::getStartType(QString ServiceName) {
  QString mReturn = "";

  //Open the service manager
  if (OpenManager()) {
    LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

    //Open the Service
    SC_HANDLE mOpenService = OpenService(mManager,mServiceName,SERVICE_QUERY_CONFIG);
    if (mOpenService != NULL) {
      //get the buffer size
      DWORD dwBytesNeeded;
      DWORD cbBufSize;
      QueryServiceConfig(mOpenService,NULL,0,&dwBytesNeeded);

      //get the service configuration
      LPQUERY_SERVICE_CONFIG lpsc;
      cbBufSize = dwBytesNeeded + 1024;

      lpsc = (LPQUERY_SERVICE_CONFIG) LocalAlloc(LMEM_FIXED, cbBufSize);
      if (QueryServiceConfig(mOpenService,lpsc,cbBufSize,&dwBytesNeeded)) {
        switch (lpsc->dwStartType) {
        case SERVICE_AUTO_START:
          mReturn = "SERVICE_AUTO_START";
          break;
        case SERVICE_BOOT_START:
          mReturn = "SERVICE_BOOT_START";
          break;
        case SERVICE_DEMAND_START:
          mReturn = "SERVICE_DEMAND_START";
          break;
        case SERVICE_DISABLED:
          mReturn = "SERVICE_DISABLED";
          break;
        case SERVICE_SYSTEM_START:
          mReturn = "SERVICE_SYSTEM_START";
          break;
        }
        LocalFree(lpsc);
      }
      //Close the Service
      CloseServiceHandle(mOpenService);
    }
    //Close the service manager
    this->CloseManager();
  }
  return mReturn;
}

//Start a Service
bool VServices::Start(QString ServiceName) {
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    //Open the service
    SC_HANDLE mService = OpenService(mManager,mServiceName,SC_MANAGER_ALL_ACCESS);
    if (mService != NULL) {
      //Attempt to start the service
      mReturn = StartService(mService,0,NULL);

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}

//Start a Service with Arguments
bool VServices::Start(QString ServiceName, QStringList Arguments) {
  if (Arguments.count() <= 0) {
    return Start(ServiceName);
  }
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    //Open the service
    SC_HANDLE mService = OpenService(mManager,mServiceName,SC_MANAGER_ALL_ACCESS);
    if (mService != NULL) {
      //Convert the QStringList to a LPCTSTR
      Arguments.insert(0,ServiceName);

      DWORD mCount = (DWORD)Arguments.count();
      LPCTSTR* pTemp = new LPCTSTR[mCount];

      for (int i = 0; i < Arguments.size(); ++i) {
        QString mValue;
        mValue += Arguments.at(i);
        mValue += '\0';
        pTemp[i] = VQTConvert::QString_To_LPCTSTR(mValue);
      }
      //Attempt to start the service
      mReturn = StartService(mService,mCount,pTemp);

      //Cleanup
      delete [] pTemp;

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}

//Stop a Service
bool VServices::Stop(QString ServiceName) {
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    //Open the service
    SC_HANDLE mService = OpenService(mManager,mServiceName,SC_MANAGER_ALL_ACCESS);
    if (mService != NULL) {
      //Attempt to stop the service
      DWORD dwControl = SERVICE_CONTROL_STOP;
      SERVICE_STATUS mStatus;
      mReturn = ControlService(mService,dwControl,&mStatus);

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}

//Set the Start Type
bool VServices::setStartType(QString ServiceName,StartType Type) {
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    //Open the service
    SC_HANDLE mService = OpenService(mManager,mServiceName,SC_MANAGER_ALL_ACCESS);
    if (mService != NULL) {
      DWORD mType = 0;
      switch (Type) {
      case  VServices::StartType_Automatic:
        mType = SERVICE_AUTO_START;
        break;

      case  VServices::StartType_Manual:
        mType = SERVICE_DEMAND_START;
        break;

      case  VServices::StartType_Disabled:
        mType = SERVICE_DISABLED;
        break;
      }

      //set the start type
      mReturn = ChangeServiceConfig(mService,SERVICE_NO_CHANGE,mType,SERVICE_NO_CHANGE,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}

//Returns a QString with the services startup account name
QString VServices::getStartAccount(QString ServiceName) {
  QString mReturn = "";

  //Open the service manager
  if (OpenManager()) {
    LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

    //Open the Service
    SC_HANDLE mOpenService = OpenService(mManager,mServiceName,SERVICE_QUERY_CONFIG);
    if (mOpenService != NULL) {
      //get the buffer size
      DWORD dwBytesNeeded;
      DWORD cbBufSize;
      QueryServiceConfig(mOpenService,NULL,0,&dwBytesNeeded);

      //get the service configuration
      LPQUERY_SERVICE_CONFIG lpsc;
      cbBufSize = dwBytesNeeded + 1024;

      lpsc = (LPQUERY_SERVICE_CONFIG) LocalAlloc(LMEM_FIXED, cbBufSize);
      if (QueryServiceConfig(mOpenService,lpsc,cbBufSize,&dwBytesNeeded)) {
        mReturn = VQTConvert::LPCTSTR_To_QString(lpsc->lpServiceStartName);

        LocalFree(lpsc);
      }
      //Close the Service
      CloseServiceHandle(mOpenService);
    }
    //Close the service manager
    CloseManager();
  }
  return mReturn;
}

//Set the services startup account name and password
bool VServices::setStartAccount(QString ServiceName, QString Username, QString Password) {
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    //Open the service
    SC_HANDLE mService = OpenService(mManager,mServiceName,SC_MANAGER_ALL_ACCESS);
    if (mService != NULL) {
      LPCTSTR mName = VQTConvert::QString_To_LPCTSTR(Username);
      LPCTSTR mPassword = VQTConvert::QString_To_LPCTSTR(Password);

      //set the start type
      mReturn = ChangeServiceConfig(mService,SERVICE_NO_CHANGE,SERVICE_NO_CHANGE,SERVICE_NO_CHANGE,NULL,NULL,NULL,NULL,mName,mPassword,NULL);

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}

//Removes a service
bool VServices::ServiceDelete(QString ServiceName) {
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    //Open the service
    SC_HANDLE mService = OpenService(mManager,mServiceName,SC_MANAGER_ALL_ACCESS);
    if (mService != NULL) {
      //delete the service
      mReturn = DeleteService(mService);

      DWORD mErr = GetLastError();

      switch (mErr) {
      case ERROR_ACCESS_DENIED:
        mReturn = false;
        break;

      case ERROR_INVALID_HANDLE:
        mReturn = false;
        break;

      case ERROR_SERVICE_MARKED_FOR_DELETE:
        mReturn = true;
        break;
      }

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}

//Creates a service
bool VServices::ServiceCreate(QString ServiceName,QString DisplayName,QString Description,StartType Type,QString Path,QString Username, QString Password) {
  bool mReturn = false;
  LPCTSTR mServiceName = VQTConvert::QString_To_LPCTSTR(ServiceName);

  //Open the service manager
  if (OpenManager()) {
    DWORD mType = 0;
    switch (Type) {
    case  VServices::StartType_Automatic:
      mType = SERVICE_AUTO_START;
      break;

    case  VServices::StartType_Manual:
      mType = SERVICE_DEMAND_START;
      break;

    case  VServices::StartType_Disabled:
      mType = SERVICE_DISABLED;
      break;
    }

    LPCTSTR mDisplayName = VQTConvert::QString_To_LPCTSTR(DisplayName);
    LPCTSTR mPath = VQTConvert::QString_To_LPCTSTR(Path);
    LPCTSTR mUsername = VQTConvert::QString_To_LPCTSTR(Username);
    LPCTSTR mPassword = VQTConvert::QString_To_LPCTSTR(Password);
    LPCTSTR mDescription = VQTConvert::QString_To_LPCTSTR(Description);

    //Create the service
    SC_HANDLE mService = CreateService(mManager,mServiceName,mDisplayName,SC_MANAGER_ALL_ACCESS,SERVICE_WIN32_OWN_PROCESS,mType,SERVICE_ERROR_NORMAL,mPath,NULL,NULL,NULL,mUsername,mPassword);
    if (mService != NULL) {
      mReturn = true;

      //set the Description
      SERVICE_DESCRIPTION sd;
      sd.lpDescription = (LPTSTR)mDescription;
      mReturn = ChangeServiceConfig2(mService,SERVICE_CONFIG_DESCRIPTION,&sd);

      //close the service
      CloseServiceHandle(mService);
    }
    //Close the service manager
    CloseManager();
  }
  //return mReturn;
  return mReturn;
}
}     //end voidrealms::win32 namespace
} //end voidrealms namespace

#endif // Q_OS_WIN
