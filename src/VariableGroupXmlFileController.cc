/** @file VariableGroupXmlFileController.cc
    @brief Implementation of the VariableGroupXmlFileController class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>
#include <QApplication>
#include <QString>
#include <QObject>

#include "AppConfig.h"
#include "VariableGroupXmlFileController.h"

VariableGroupXmlFileController* VariableGroupXmlFileController::s_pSingletonObject = NULL;

VariableGroupXmlFileController* VariableGroupXmlFileController::GetSingleton() {
  if (s_pSingletonObject != NULL) {
    ;
  } else {
    s_pSingletonObject = new VariableGroupXmlFileController();
  }
  return s_pSingletonObject;
}

void VariableGroupXmlFileController::ReleaseSingleton() {
  if (s_pSingletonObject) {
    delete s_pSingletonObject;
    s_pSingletonObject = NULL;
  }
}

VariableGroupXmlFileController::VariableGroupXmlFileController(void)
{}

VariableGroupXmlFileController::~VariableGroupXmlFileController(void)
{}

bool VariableGroupXmlFileController::LoadVariablesGroups(QList<VariableGroup>* pTarget, const QString &sXmlFileName, const QString &sSchemaFileName, QString & sLastError) {
  sLastError = "";
  if (!ValidateVariablesGroupsFile(sXmlFileName, sSchemaFileName, sLastError)) {
    return false;
  }
  QFile variableFile(sXmlFileName);
  if (pTarget == NULL) {
    sLastError = QString(tr("Could not open xml file: "));
    sLastError += sXmlFileName;
    return false;
  }
  QXmlStreamReader reader;
  pTarget->clear();
  bool read = variableFile.open(QIODevice::ReadOnly | QIODevice::Text);
  if (!read) {
    sLastError = variableFile.errorString();
    return false;
  } else {
    reader.setDevice(&variableFile);
    while (!reader.atEnd())
    {
      reader.readNext();
      if (reader.isStartElement() && reader.name() == "groups") {
        bool bIsEndGroups = false;
        int count = 0;
        while (!reader.atEnd() && !bIsEndGroups)
        {
          reader.readNext();
          QString ss = reader.name().toString();
          if (reader.tokenType() == QXmlStreamReader::StartElement
              && reader.name() == "group") {
            count++;
            QString id = reader.attributes().value("id").toString();
            VariableGroup newGroup;
            this->LoadGroup(&reader, &newGroup);
            pTarget->append(newGroup);
          } else if (reader.tokenType() == QXmlStreamReader::EndDocument
                     || (reader.tokenType() == QXmlStreamReader::EndElement && reader.name() == AppConfig::s_sXmlTag_Groups)) {
            bIsEndGroups = true;
          }
        }
      }
    }
  }
  variableFile.close();
  return true;
}

bool VariableGroupXmlFileController::LoadUOMValuesList(const QString &sSchemaFileName, QStringList &ListValues, QString &sLastError) {
  sLastError = "";
  ListValues.clear();
  QFile schemaFile(sSchemaFileName);
  if (schemaFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QXmlStreamReader reader;
    reader.setDevice(&schemaFile);
    bool bIsDone = false;
    while (!reader.atEnd() && !bIsDone) {
      reader.readNext();
      if (reader.isStartElement()
          && reader.name() == "simpleType"
          && reader.attributes().value("name").toString() == "UnitsOfMeasurements") {
        reader.readNextStartElement();
        // Pass "restriction" tag
        reader.readNextStartElement();
        while (reader.name() == "enumeration") {
          if (reader.tokenType() == QXmlStreamReader::StartElement) {
            QString newValue = reader.attributes().value("value").toString();
            ListValues.append(newValue);
          }
          reader.readNextStartElement();
        }
        bIsDone = true;
      }
    }
    schemaFile.close();
  } else {
    sLastError = QString(tr("Could not open schema file: "));
    sLastError += sSchemaFileName;
  }
  return true;
}

bool VariableGroupXmlFileController::SaveVariablesGroups(QList<VariableGroup>* pTarget, const QString & sFileName, QString & sLastError) {
  sLastError = "";
  QFile variableFile(sFileName);
  if (pTarget == NULL) {
    sLastError = "Variable groups list is empty";
    return false;
  }
  QXmlStreamWriter writer;
  bool read = variableFile.open(QIODevice::WriteOnly | QIODevice::Text);
  if (!read) {
    sLastError = variableFile.errorString();
    return false;
  } else {
    // Setup writer
    writer.setDevice(&variableFile);
    writer.setAutoFormatting(true);
    writer.setAutoFormattingIndent(2);

    writer.setCodec(AppConfig::s_sVariableGroupsFileXmlEncoding.toLatin1().data());
    writer.writeStartDocument(AppConfig::s_sVariableGroupsFileXmlVersion);
    writer.writeProcessingInstruction("xml-stylesheet", AppConfig::s_sXmlProcessingInstruction_StyleSheet);
    writer.writeStartElement(AppConfig::s_sXmlTag_Groups);
    writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupsLink, AppConfig::s_sXmlAttributeValue_GroupsLink);
    writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupsSchema, AppConfig::s_sXmlAttributeValue_GroupsSchema);
    // int count = pTarget->length();
    for (int vgi = 0; vgi < pTarget->length(); vgi++) {
      QString id = pTarget->at(vgi).m_sId;
      writer.writeStartElement(AppConfig::s_sXmlTag_Group);
      writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupId, pTarget->at(vgi).m_sId);
      pTarget->at(vgi).m_bIsEnabled
      ? writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupEnabled, QString("true"))
      : writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupEnabled, QString("false"));
      writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupDescription, pTarget->at(vgi).m_sDescription);
      writer.writeAttribute(AppConfig::s_sXmlAttribute_GroupModel, pTarget->at(vgi).m_sModel);

      for (int vii = 0; vii < pTarget->at(vgi).m_listVariablesInput.length(); vii++) {
        writer.writeStartElement(AppConfig::s_sXmlTag_Input);

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableOPCtag);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesInput[vii].m_sOPCtag);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableDescription);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesInput[vii].m_sDescription);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableTAG);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesInput[vii].m_sTag);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableUOM);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesInput[vii].m_sUOM);
        writer.writeEndElement();

        if (pTarget->at(vgi).m_listVariablesInput[vii].m_dMin != Variable::s_dUndefinedValue) {
          writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableMin);
          writer.writeCharacters(QString::number(pTarget->at(vgi).m_listVariablesInput[vii].m_dMin));
          writer.writeEndElement();
        }
        if (pTarget->at(vgi).m_listVariablesInput[vii].m_dMax != Variable::s_dUndefinedValue) {
          writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableMax);
          writer.writeCharacters(QString::number(pTarget->at(vgi).m_listVariablesInput[vii].m_dMax));
          writer.writeEndElement();
        }
        writer.writeEndElement();
      }
      for (int vri = 0; vri < pTarget->at(vgi).m_listVariablesResult.length(); vri++) {
        writer.writeStartElement(AppConfig::s_sXmlTag_Result);

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableOPCtag);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesResult[vri].m_sOPCtag);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableDescription);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesResult[vri].m_sDescription);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableTAG);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesResult[vri].m_sTag);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableUOM);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesResult[vri].m_sUOM);
        writer.writeEndElement();

        writer.writeEndElement();
      }
      for (int vci = 0; vci < pTarget->at(vgi).m_listVariablesConstant.length(); vci++) {
        writer.writeStartElement(AppConfig::s_sXmlTag_Constant);

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableOPCtag);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesConstant[vci].m_sOPCtag);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableDescription);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesConstant[vci].m_sDescription);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableTAG);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesConstant[vci].m_sTag);
        writer.writeEndElement();

        writer.writeStartElement(AppConfig::s_sXmlAttribute_VariableUOM);
        writer.writeCharacters(pTarget->at(vgi).m_listVariablesConstant[vci].m_sUOM);
        writer.writeEndElement();

        writer.writeEndElement();
      }
      writer.writeEndElement();
    }
    writer.writeEndElement();
    writer.writeEndDocument();
    variableFile.flush();
    variableFile.close();
  }
  return true;
}

bool VariableGroupXmlFileController::ValidateVariablesGroupsFile(const QString &sXmlFileName, const QString &sSchemaFileName, QString &sLastError) {
  bool bIsValidated = false;
  sLastError = "";
  QFile xmlFile(sXmlFileName);
  if (xmlFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QFile schemaFile(sSchemaFileName);
    if (schemaFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
      MessageHandler messageHandler;
      QXmlSchema pSchema;
      pSchema.setMessageHandler(&messageHandler);
      if (pSchema.load(&schemaFile)) {
        QXmlSchemaValidator* pValidator = new QXmlSchemaValidator(pSchema);
        if (pValidator->validate(&xmlFile)) {
          bIsValidated = true;
        } else {
          sLastError = QString(tr("Xml validation failed: "));
          sLastError += messageHandler.statusMessage();
        }
      } else {
        sLastError = QString(tr("Could not load schema file."));
      }
    }
    else {
      sLastError = QString(tr("Could not open schema file: "));
      sLastError += sSchemaFileName;
    }
  } else {
    sLastError = QString(tr("Could not open xml file: "));
    sLastError += sXmlFileName;
  }
  return bIsValidated;
}

void VariableGroupXmlFileController::LoadGroup(QXmlStreamReader* pReader,VariableGroup* NewGroup) {
  NewGroup->m_sId = pReader->attributes().value("id").toString();
  NewGroup->m_sDescription = pReader->attributes().value("description").toString();
  NewGroup->m_sModel = pReader->attributes().value("model").toString();
  NewGroup->m_bIsEnabled = pReader->attributes().value("enabled").toString() == "true";
  // Load inputs;
  bool bEndGroup = false;
  while (!bEndGroup && !pReader->atEnd())
  {
    pReader->readNext();
    QString ss = pReader->name().toString();
    // QXmlStreamReader::TokenType tt = pReader->tokenType();
    if (pReader->name() == "group") {
      // int i = 0;
    }
    if (pReader->tokenType() == QXmlStreamReader::EndElement
        && pReader->name() == "group") {
      bEndGroup = true;
    } else {
      if (pReader->name() == "input") {
        LoadInput(pReader, NewGroup);
      } else if (pReader->name() == "result") {
        LoadResult(pReader, NewGroup);
      } else if (pReader->name() == "constant") {
        LoadConstant(pReader, NewGroup);
      }
    }
  }
}

void VariableGroupXmlFileController::LoadInput(QXmlStreamReader* pReader, VariableGroup* Owner) {
  bool bIsEndInput = false;
  Variable newVar;
  while (!bIsEndInput && !pReader->atEnd())
  {
    pReader->readNext();
    QString ss = pReader->name().toString();
    if (pReader->tokenType() == QXmlStreamReader::EndElement
        && pReader->name() == "input") {
      bIsEndInput = true;
    } else {
      newVar.m_eType = Variable::VT_INPUT;
      if (pReader->name() == "OPCtag") {
        newVar.m_sOPCtag = pReader->readElementText();
      } else if (pReader->name() == "description") {
        newVar.m_sDescription = pReader->readElementText();
      } else if (pReader->name() == "TAG") {
        newVar.m_sTag = pReader->readElementText();
      } else if (pReader->name() == "UOM") {
        newVar.m_sUOM = pReader->readElementText();
      } else if (pReader->name() == "min") {
        newVar.m_dMin = pReader->readElementText().toDouble();
      } else if (pReader->name() == "max") {
        newVar.m_dMax = pReader->readElementText().toDouble();
      }
    }
  }
  Owner->m_listVariablesInput.append(newVar);
}

void VariableGroupXmlFileController::LoadResult(QXmlStreamReader* pReader,VariableGroup* Owner) {
  bool bIsEndResult = false;
  Variable newVar;
  while (!bIsEndResult && !pReader->atEnd())
  {
    pReader->readNext();
    if (pReader->tokenType() == QXmlStreamReader::EndElement
        && pReader->name() == "result") {
      bIsEndResult = true;
    } else {
      newVar.m_eType = Variable::VT_RESULT;
      if (pReader->name() == "OPCtag") {
        newVar.m_sOPCtag = pReader->readElementText();
      } else if (pReader->name() == "description") {
        newVar.m_sDescription = pReader->readElementText();
      } else if (pReader->name() == "TAG") {
        newVar.m_sTag = pReader->readElementText();
      } else if (pReader->name() == "UOM") {
        newVar.m_sUOM = pReader->readElementText();
      }
    }
  }
  Owner->m_listVariablesResult.append(newVar);
}

void VariableGroupXmlFileController::LoadConstant(QXmlStreamReader* pReader, VariableGroup* Owner) {
  bool bIsEndConstant = false;
  Variable newVar;
  while (!bIsEndConstant && !pReader->atEnd())
  {
    pReader->readNext();

    if (pReader->tokenType() == QXmlStreamReader::EndElement
        && pReader->name() == "constant") {
      bIsEndConstant = true;
    } else {
      newVar.m_eType = Variable::VT_CONSTANT;
      if (pReader->name() == "OPCtag") {
        newVar.m_sOPCtag = pReader->readElementText();
      } else if (pReader->name() == "description") {
        newVar.m_sDescription = pReader->readElementText();
      } else if (pReader->name() == "TAG") {
        newVar.m_sTag = pReader->readElementText();
      } else if (pReader->name() == "UOM") {
        newVar.m_sUOM = pReader->readElementText();
      }
    }
  }
  Owner->m_listVariablesConstant.append(newVar);
}
