/** @file VariableInputDelegate.cc
    @brief Implementation of the VariableInputDelegate class

    This file is part of LOC, the LIBPF OPC Configurator

    All rights reserved.
    @author (C) Copyright 2009-2016 Paolo Greppi simevo s.r.l.

    Based in part on code from Hung Pham.

    Developed for Qt 5.6 Open Source Edition - Copyright (C) 2015 The Qt Company Ltd.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QComboBox>
#include <QLineEdit>

#include "VariableInputDelegate.h"

VariableInputDelegate::VariableInputDelegate(QStringList ListUOMValues) {
  this->m_listUOMValues = ListUOMValues;
}

VariableInputDelegate::~VariableInputDelegate(void)
{}

QWidget* VariableInputDelegate::createEditor(QWidget *parent
                                             , const QStyleOptionViewItem & /*option */
                                             , const QModelIndex &index) const {
  if (index.column() == 3) {
    QComboBox* editor = new QComboBox(parent);
    editor->addItems(this->m_listUOMValues);
    return editor;
  } else {
    QLineEdit* editor = new QLineEdit(parent);
    return editor;
  }
}

void VariableInputDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
  if (index.column() == 3) {
    QString value = index.model()->data(index, Qt::EditRole).toString();
    int indexUom = this->m_listUOMValues.indexOf(value);
    if (indexUom == -1) {
      indexUom = 0;
    }
    QComboBox* UOMCbb = static_cast<QComboBox*>(editor);
    UOMCbb->setCurrentIndex(indexUom);
  } else {
    QString value = index.model()->data(index, Qt::EditRole).toString();
    QLineEdit* line = static_cast<QLineEdit*>(editor);
    line->setText(value);
  }
}

void VariableInputDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                         const QModelIndex &index) const {
  if (index.column() == 3) {
    QComboBox *UOMCombobox = static_cast<QComboBox*>(editor);
    model->setData(index, UOMCombobox->currentText(), Qt::EditRole);
  } else {
    QLineEdit* line = static_cast<QLineEdit*>(editor);
    QString t = line->text();
    model->setData(index, t, Qt::EditRole);
  }
}

void VariableInputDelegate::updateEditorGeometry(QWidget *editor
                                                 , const QStyleOptionViewItem &option
                                                 , const QModelIndex &/* index */) const {
  editor->setGeometry(option.rect);
}
