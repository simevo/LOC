# enable to compile with LLVM's clang
# QMAKE_CC=clang
# QMAKE_CXX=clang

# enable to compile with color highlighted warnings and errors on linux
# QMAKE_CC=colorgcc
# QMAKE_CXX=colorgcc

CONFIG += debug_and_release
CONFIG(debug, debug|release) {
  TARGET = LOC_debug
}
# add QT_NO_DEBUG_OUTPUT to turn off qDebug() messages
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
## add QT_NO_WARNING_OUTPUT to turn off qWarning() messages
#CONFIG(release, debug|release):DEFINES += QT_NO_WARNING_OUTPUT

Release:DESTDIR = ../bin/Release.LOC
Release:OBJECTS_DIR = ../bin/Release.LOC/.obj
Release:MOC_DIR = ../bin/Release.LOC/.moc
Release:RCC_DIR = ../bin/Release.LOC/.rcc
Release:UI_DIR = ../bin/Release.LOC/.ui

Debug:DESTDIR = ../bin/Debug.LOC
Debug:OBJECTS_DIR = ../bin/Debug.LOC/.obj
Debug:MOC_DIR = ../bin/Debug.LOC/.moc
Debug:RCC_DIR = ../bin/Debug.LOC/.rcc
Debug:UI_DIR = ../bin/Debug.LOC/.ui 

INCLUDEPATH  += include

QT           += svg
QT           += xml
QT           += xmlpatterns
greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
}

HEADERS      += include/AppConfig.h \
  include/FolderPathValidator.h \
  include/LibpfOpcConfigurations.h \
  include/MainWidget.h \
  include/MySettings.h \
  include/TabConfigure.h \
  include/TabControl.h \
  include/TabVariables.h \
  include/VariableGroup.h \
  include/VariableGroupXmlFileController.h \
  include/Variable.h \
  include/VariableInputDelegate.h \
  include/vqtconvert.h \
  include/vservices.h \
  include/MainWindow.h

SOURCES      += src/AppConfig.cc \
  src/FolderPathValidator.cc \
  src/LibpfOpcConfigurations.cc \
  src/main.cc \
  src/MySettings.cc \
  src/MainWidget.cc \
  src/TabConfigure.cc \
  src/TabControl.cc \
  src/TabVariables.cc \
  src/Variable.cc \
  src/VariableGroup.cc \
  src/VariableGroupXmlFileController.cc \
  src/VariableInputDelegate.cc \
  src/vqtconvert.cc \
  src/vservices.cc \
    src/MainWindow.cc

win32 {
  RC_FILE      = LOC.rc
  LIBS += advapi32.lib
  QMAKE_CXXFLAGS += /FS
  QMAKE_LFLAGS += "/MANIFESTUAC:level='requireAdministrator'"
  CONFIG += embed_manifest_exe
}

RESOURCES    = loc.qrc
