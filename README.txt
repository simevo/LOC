This is LOC (LIBPF� OPC Configurator) version 1.0.2264.

The LOC homepage is https://gitlab.com/simevo/LOC

LOC, the LIBPF� OPC Configurator, provides the User Interface to configure solutions developed with the LIBPF OPC module (http://libpf.com/sdk/libpf-opc.html) of LIBPF�, the LIBrary for Process Flowsheeting in C++.

For more informations please visit the LIBPF� website at http://www.libpf.com

Credits
=======
(C) Copyright 2009-2016 Paolo Greppi simevo s.r.l. - All rights reserved.

License
=======
This program may be used under the terms of the GNU General Public License version 2.0 as published by the Free Software Foundation and appearing in the file GPL_LICENSE.txt included in the packaging of this file.
